# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from administration.forms.agent import MairieAgentForm
from administration.models.agent import CGAgent, GGDAgent, DDSPAgent, EDSRAgent, BrigadeAgent, SDISAgent, CODISAgent, CISAgent, ServiceAgent, \
    FederationAgent, MairieAgent, CGSuperieur
from core.util.admin import set_admin_info, RelationOnlyFieldListFilter


@admin.register(CGAgent, CGSuperieur)
class CGAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin CGAgent et CGSupérieur """

    # Configuration
    list_display = ['pk', 'agent', 'cg', 'cg__departement']
    list_filter = [('cg__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cg__departement=request.user.get_departement())
        return queryset


@admin.register(GGDAgent)
class GGDAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'ggd', 'ggd__departement']
    list_filter = [('ggd__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(ggd__departement=request.user.get_departement())
        return queryset


@admin.register(EDSRAgent)
class EDSRAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'edsr', 'edsr__departement']
    list_filter = [('edsr__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(edsr__departement=request.user.get_departement())
        return queryset


@admin.register(DDSPAgent)
class DDSPAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'ddsp', 'ddsp__departement']
    list_filter = [('ddsp__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(ddsp__departement=request.user.get_departement())
        return queryset


@admin.register(SDISAgent)
class SDISAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'sdis', 'sdis__departement']
    list_filter = [('sdis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(sdis__departement=request.user.get_departement())
        return queryset


@admin.register(CODISAgent)
class CODISAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'codis', 'codis__departement']
    list_filter = [('codis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(codis__departement=request.user.get_departement())
        return queryset


@admin.register(CISAgent)
class CISAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'cis', 'cis__commune__arrondissement__departement']
    list_filter = ['cis', ('cis__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cis__commune__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(ServiceAgent)
class ServiceAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'service', 'service__departement']
    list_filter = [('service__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(service__departement=request.user.get_departement())
        return queryset


@admin.register(FederationAgent)
class FederationAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'federation']
    list_filter = ['federation']
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(federation__departement=request.user.get_departement())
        return queryset


@admin.register(MairieAgent)
class MairieAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'commune', 'get_departement']
    list_filter = [('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    form = MairieAgentForm
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(BrigadeAgent)
class BrigadeAgentAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent', 'brigade', 'get_departement']
    list_filter = [('brigade__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(brigade__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    # Getter
    @set_admin_info(admin_order_field='brigade__commune__arrondissement__departement', short_description="Département")
    def get_departement(self, obj):
        return obj.brigade.commune.arrondissement.departement
