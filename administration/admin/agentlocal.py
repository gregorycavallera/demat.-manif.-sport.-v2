# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from administration.models.agentlocal import CGServiceAgentLocal, CGDAgentLocal, CompagnieAgentLocal, CommissariatAgentLocal, \
    EDSRAgentLocal
from core.util.admin import RelationOnlyFieldListFilter


@admin.register(CGServiceAgentLocal)
class CGServiceAgentLocalAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'cg_service', 'cg_service__cg__departement']
    list_filter = ['cg_service', ('cg_service__cg__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cg_service__cg__departement=request.user.get_departement())
        return queryset


@admin.register(CompagnieAgentLocal)
class CompagnieAgentLocalAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'compagnie', 'compagnie__sdis__departement']
    list_filter = ['compagnie', ('compagnie__sdis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(compagnie__sdis__departement=request.user.get_departement())
        return queryset


@admin.register(CommissariatAgentLocal)
class CommissariatAgentLocalAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'commissariat', 'commissariat__commune__arrondissement__departement']
    list_filter = ['commissariat', ('commissariat__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commissariat__commune__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(CGDAgentLocal)
class CGDAgentLocalAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'cgd', 'cgd__arrondissement__departement']
    list_filter = ['cgd', ('cgd__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cgd__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(EDSRAgentLocal)
class EDSRAgentLocalAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'edsr', 'edsr__departement']
    list_filter = [('edsr__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(edsr__departement=request.user.get_departement())
        return queryset
