# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from administration.models.people import Instructeur, Secouriste
from core.util.admin import RelationOnlyFieldListFilter


@admin.register(Instructeur)
class InstructeurAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'user', 'prefecture', 'prefecture__arrondissement__departement']
    list_filter = [('prefecture__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(prefecture__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(Secouriste)
class SecouristeAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'user', 'association']
    list_filter = ['association']
    search_fields = ['user__username']
    list_per_page = 50
