# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from administration.models.service import Prefecture, CGService, Service, EDSR, SDIS, DDSP, CG, GGD, \
    CODIS, Brigade, CGD, Compagnie, Commissariat, CIS
from core.util.admin import RelationOnlyFieldListFilter


@admin.register(Prefecture)
class PrefectureAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'arrondissement', 'get_departement', 'email']
    search_fields = ['email', 'departement__name']
    list_filter = [('arrondissement__departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(Service)
class ServiceAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'name']
    search_fields = ['name']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset


@admin.register(DDSP, CG)
class DepartementAndEmailAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'email']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset



@admin.register(EDSR, GGD, SDIS, CODIS)
class DepartementOnlyAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset


@admin.register(Commissariat)
class CommuneOnlyAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_commune', 'get_departement']
    list_filter = [('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(CIS)
class CISAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'get_commune', 'get_departement', 'compagnie']
    list_filter = ['compagnie', ('compagnie__sdis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(compagnie__sdis__departement=request.user.get_departement())
        return queryset


@admin.register(Brigade)
class BrigadeAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_commune', 'get_departement', 'kind', 'cgd']
    list_filter = ['kind', ('cgd__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['commune__name']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cgd__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(CGD)
class CGDAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'arrondissement', 'get_departement']
    list_filter = [('arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['arrondissement__name', 'arrondissement__departement__name']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(Compagnie)
class CompagnieAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'sdis', 'number']
    list_filter = [('sdis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['number']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(sdis__departement=request.user.get_departement())
        return queryset


@admin.register(CGService)
class CGServiceAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'cg', 'service_type', 'cg__departement']
    list_filter = ['service_type', ('cg__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cg__departement=request.user.get_departement())
        return queryset
