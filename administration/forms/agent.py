# coding: utf-8
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from django import forms
from administration.models.agent import MairieAgent
from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField
from core.forms.base import GenericForm


class MairieAgentForm(GenericForm):
    """ Formulaire des structures """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Mairie")

    # Overrides
    def __init__(self, *args, **kwargs):
        super(MairieAgentForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['commune'].empty_label = Commune.objects.get(id=self.initial['commune'])

    def is_valid(self):
        bool = True
        if self.instance.pk:
            if self.data['commune'] == '':
                self.data['commune'] = Commune.objects.get(id=self.initial['commune']).pk
        status = super(MairieAgentForm,self).is_valid()
        return status

    # Meta
    class Meta:
        model = MairieAgent
        fields = ['user', 'departement', 'commune']
