# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import OnePerDepartementMixin, ManyPerDepartementMixin, OnePerArrondissementMixin, ManyPerCommuneMixin, \
    OnePerDepartementQuerySetMixin, OnePerArrondissementQuerySetMixin, ManyPerCommuneQuerySetMixin, ManyPerDepartementQuerySetMixin


class GGD(OnePerDepartementMixin):
    """ Groupe départemental de gendarmerie """

    # Champs
    objects = OnePerDepartementQuerySetMixin.as_manager()

    def __str__(self):
        return ' '.join(['GGD', self.get_departement().name])

    def get_ggdagents(self):
        """ Renvoyer les agents du GGD """
        return self.ggdagents.all()

    class Meta:
        verbose_name = "GGD"
        verbose_name_plural = "GGD"
        app_label = 'administration'
        # default_related_name = 'ggds'


class CG(OnePerDepartementMixin):
    """ Conseil départemental """

    # Champs
    email = models.EmailField("e-mail", max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    def __str__(self):
        return ' '.join(['CD', self.get_departement().name])

    def get_cgsuperieurs(self):
        """ Renvoyer les agents n+1 du CG """
        return self.cgsuperieurs.all()

    def get_cgagents(self):
        """ Renvoyer les agents n du CG """
        return self.cgagents.all()

    class Meta:
        verbose_name = "CD"
        verbose_name_plural = "CD"
        app_label = 'administration'
        # default_related_name = 'cgs'


class DDSP(OnePerDepartementMixin):
    """ Direction départementale de sécurité publique """

    # Champs
    email = models.EmailField("e-mail", max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    def __str__(self):
        return ' '.join(['DDSP', self.get_departement().name])

    def get_ddspagents(self):
        """ Renvoyer les agents DDSP """
        return self.ddspagents.all()

    class Meta:
        verbose_name = 'DDSP'
        verbose_name_plural = 'DDSP'
        app_label = 'administration'
        # default_related_name = 'ddsps'


class SDIS(OnePerDepartementMixin):
    """ Service départemental incendies et secourisme """

    # Champs
    objects = OnePerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['SDIS', self.get_departement().name])

    # Getter
    def get_sdisagents(self):
        """ Renvoyer les agents SDIS """
        return self.sdisagents.all()

    class Meta:
        verbose_name = 'SDIS'
        verbose_name_plural = 'SDIS'
        app_label = 'administration'
        # default_related_name = 'sdiss'


class CODIS(OnePerDepartementMixin):
    """ SDIS : Centre d'opérations départemental incendies et secourisme """

    # Champs
    objects = OnePerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['CODIS', self.get_departement().name])

    # Getter
    def get_codisagents(self):
        """ Renvoyer les agents codis """
        return self.codisagents.all()

    class Meta:
        verbose_name = 'SDIS - CODIS'
        verbose_name_plural = 'SDIS - CODIS'
        app_label = 'administration'
        # default_related_name = 'codiss'


class EDSR(OnePerDepartementMixin):
    """ Escadron départemental de sécurité routière """

    # Champs
    objects = OnePerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['EDSR', self.get_departement().name])

    # Getter
    def get_edsr_agents(self):
        """ Renvoyer les agents EDSR """
        return self.edsragents.all()

    class Meta:
        verbose_name = 'EDSR'
        verbose_name_plural = 'EDSR'
        app_label = 'administration'
        # default_related_name = 'edsrs'


class Prefecture(OnePerArrondissementMixin):
    """ Préfécture (et sous-préfectures) """

    # Champs
    sous_prefecture = models.BooleanField("Sous-préfecture", default=False)
    email = models.EmailField('e-mail', max_length=200)
    objects = OnePerArrondissementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        typename = "sous-préfecture" if self.sous_prefecture else "préfecture"
        return ' '.join([typename.capitalize(), 'de', self.arrondissement.name])

    # Getter
    def get_instructeurs(self):
        """
        Renvoyer les instructeurs actifs pour la (sous-)préfecture
        
        Note : quand le mode d'instruction d'une instance/département est
        défini à 'Départemental', tous les instructeurs de toutes les
        sous-préfectures/arrondissements du département sont toujours concernés
        tous ensemble par les événements du département.
        """
        from administration.models import Instructeur
        return Instructeur.objects.by_prefecture(self)

    # Meta
    class Meta:
        verbose_name = 'Préfecture'
        verbose_name_plural = 'Préfectures'
        app_label = 'administration'
        abstract = False
        # default_related_name = 'prefectures'


class CGD(OnePerArrondissementMixin):
    """ Compagnie de gendarmerie départementale """

    # Champs
    objects = OnePerArrondissementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' - '.join(['CGD', self.arrondissement.name])

    class Meta:
        verbose_name = 'CGD'
        verbose_name_plural = 'CGD'
        app_label = 'administration'
        # default_related_name = 'cgds'


class Brigade(ManyPerCommuneMixin):
    """ Brigade EDSR (Escadron de sécurité routière) """

    # Constantes
    KIND_CHOICES = (('cob', 'Brigade de communauté'), ('bta', 'Brigade autonome'))

    # Champs
    kind = models.CharField('type', max_length=12, choices=KIND_CHOICES)
    cgd = models.ForeignKey('administration.cgd', verbose_name='CGD', on_delete=models.CASCADE)
    objects = ManyPerCommuneQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return '{0} - {1} ({2})'.format(self.kind.upper(), self.commune.name, self.commune.get_departement().name)

    # Méta
    class Meta:
        verbose_name = 'EDSR - Brigade'
        verbose_name_plural = 'EDSR - Brigades'
        app_label = 'administration'
        default_related_name = 'brigades'


class Compagnie(models.Model):
    """ Compagnie SDIS (Incendie et secourisme) """

    # Champs
    sdis = models.ForeignKey('administration.sdis', verbose_name='SDIS', on_delete=models.CASCADE)
    number = models.CharField("Numéro de compagnie/groupement", max_length=12)

    # Overrides
    def __str__(self):
        return '{0} {1} ({2})'.format('compagnie/groupement', self.number, self.sdis.departement.name)

    def natural_key(self):
        return self.sdis.departement.name, self.number

    # Getter
    def get_compagnieagentslocaux(self):
        """ Renvoyer les agents locaux """
        return self.compagnieagentslocaux.all()

    # MEta
    class Meta:
        verbose_name = 'SDIS - Compagnie/Groupement'
        verbose_name_plural = 'SDIS - Compagnies/Groupements'
        ordering = ['number']
        app_label = 'administration'
        default_related_name = 'compagnies'


class CIS(ManyPerCommuneMixin):
    """ Caserne / Service feu """

    # Champs
    name = models.CharField("Nom", max_length=255, blank=True)
    compagnie = models.ForeignKey('administration.compagnie', verbose_name='compagnie/groupement', on_delete=models.CASCADE)
    objects = ManyPerCommuneQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(filter(None, ['CIS', self.commune.name.capitalize(), self.name]))

    # Méta
    class Meta:
        verbose_name = 'SDIS - CIS'
        verbose_name_plural = 'SDIS - CIS'
        app_label = 'administration'
        default_related_name = 'ciss'


class Commissariat(ManyPerCommuneMixin):
    """ Commissariat de police """

    # Champs
    objects = ManyPerCommuneQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['commissariat', self.commune.name])

    # Meta
    class Meta:
        verbose_name = 'DDSP - Commissariat'
        verbose_name_plural = 'DDSP - Commissariats'
        app_label = 'administration'
        default_related_name = 'commissariats'


class Service(ManyPerDepartementMixin):
    """ Autre service """

    # Champs
    name = models.CharField("Nom", max_length=255, blank=False)
    objects = ManyPerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' - '.join([self.get_departement().name, self.name])

    # Getter
    def get_serviceagents(self):
        """ Renvoyer les agents """
        return self.serviceagents.all()

    # Meta
    class Meta:
        verbose_name = "Autre service"
        verbose_name_plural = "Autres services"
        unique_together = [['name', 'departement']]
        app_label = 'administration'
        default_related_name = 'services'


class CGServiceQuerySet(models.QuerySet):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, pk, name):
        return self.get(id=pk, cg__departement__name=name)


class CGService(models.Model):
    """ Service de conseil départemental """

    # Constantes
    SERVICE_TYPE_CHOICES = (
        ('STD', 'Service technique départemental'),
        ('DTM', 'Direction des transports et de la mobilité'),
    )
    # Champs
    name = models.CharField("Nom", max_length=255)
    cg = models.ForeignKey('administration.cg', verbose_name='CD', on_delete=models.CASCADE)
    service_type = models.CharField("type de service", max_length=3, choices=SERVICE_TYPE_CHOICES)
    objects = CGServiceQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join([self.service_type, self.name])

    def natural_key(self):
        return (self.pk,) + self.cg.natural_key()

    # Meta
    class Meta:
        verbose_name = 'CD - Service'
        verbose_name_plural = 'CD - Services'
        app_label = 'administration'
        default_related_name = 'cgservices'
