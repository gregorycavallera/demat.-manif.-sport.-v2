# coding: utf-8
from clever_selects.views import ChainedSelectChoicesView
from localflavor.fr.fr_department import DEPARTMENT_CHOICES_PER_REGION_2016

from administration.models.service import *
from administrative_division.models.commune import Commune
from core.models import Instance
from sports.models import Federation
from emergencies.models import Association1ersSecours


class ServiceAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les activités selon le champ parent (Discipline) sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = list(Service.objects.filter(departement_id=int(self.parent_value)).values_list('id', 'name'))
        elif isinstance(self.parent_value, (list, tuple)):
            result = list(Service.objects.filter(departement_id__in=self.parent_value).values_list('id', 'name'))
        else:
            result = []
        return result


class CommuneAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les communes selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Commune.objects.filter(arrondissement__departement=departement)
            for commune in liste:
                result.append((str(commune.id), commune.name + ' - ' + commune.zip_code))
        else:
            result = []
        return result


class PrefectureAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les préfectures selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Prefecture.objects.filter(arrondissement__departement=departement)
            for prefecture in liste:
                result.append((str(prefecture.id), prefecture.__str__()))
        else:
            result = []
        return result


class CGServiceAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            result = list(CGService.objects.filter(cg__departement=departement).values_list('id', 'name'))
        else:
            result = []
        return result


class CGDAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = CGD.objects.filter(arrondissement__departement=departement)
            for cgd in liste:
                result.append((str(cgd.id), cgd.__str__()))
        else:
            result = []
        return result


class BrigadeAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Brigade.objects.filter(cgd__arrondissement__departement=departement)
            for brigade in liste:
                result.append((str(brigade.id), brigade.__str__()))
        else:
            result = []
        return result


class CommissariatAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Commissariat.objects.filter(commune__arrondissement__departement=departement)
            for commissariat in liste:
                result.append((str(commissariat.id), commissariat.__str__()))
        else:
            result = []
        return result


class CompagnieAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Compagnie.objects.filter(sdis__departement=departement)
            for compagnie in liste:
                result.append((str(compagnie.id), compagnie.__str__()))
        else:
            result = []
        return result


class CISAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = CIS.objects.filter(compagnie__sdis__departement=departement)
            for cis in liste:
                result.append((str(cis.id), cis.__str__()))
        else:
            result = []
        return result


class AutreServiceAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Service.objects.filter(departement=departement)
            for autre in liste:
                result.append((str(autre.id), autre.__str__()))
        else:
            result = []
        return result


class FederationAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les fédérations selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            for dept in DEPARTMENT_CHOICES_PER_REGION_2016:
                if dept[0] == departement.name:
                    region = dept[2]
            liste1 = Federation.objects.filter(level=Federation.DEPARTEMENTAL, departement=departement)
            liste2 = Federation.objects.filter(level=Federation.REGIONAL, region=region)
            liste3 = Federation.objects.filter(level=Federation.NATIONAL)
            liste = liste3 | liste2 | liste1
            for fede in liste:
                result.append((str(fede.id), fede.name))
        else:
            result = []
        return result


class SecoursAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services de secours selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste1 = Association1ersSecours.objects.filter(departement=departement)
            # TODO : A vérifier si normal que des objets ne soit pas rattachés à un département
            liste2 = Association1ersSecours.objects.filter(departement__isnull=True)
            liste = liste2 | liste1
            for secours in liste:
                result.append((str(secours.id), secours.__str__()))
        else:
            result = []
        return result
