# coding: utf-8
from django import forms
from localflavor.fr.forms import FRZipCodeField, FRDepartmentField

from administrative_division.models import Commune, Departement, Arrondissement


class CommuneForm(forms.ModelForm):
    """ Formulaire des communes """
    zip_code = FRZipCodeField()

    class Meta:
        model = Commune
        fields = '__all__'


class ArrondissementForm(forms.ModelForm):
    """ Formulaire des Arrondissements """

    class Meta:
        model = Arrondissement
        fields = '__all__'


class DepartementForm(forms.ModelForm):
    """ Formulaire des départements """
    name = FRDepartmentField()

    class Meta:
        model = Departement
        fields = '__all__'
