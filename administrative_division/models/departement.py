# coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from localflavor.fr.fr_department import DEPARTMENT_CHOICES


class DepartementManager(models.Manager):
    """ Manager """

    def get_by_natural_key(self, name):
        return self.get(name=name)

    def configured(self):
        """ Renvoyer les départements ayant une instance configurée """
        return self.filter(instance__isnull=False)

    def unconfigured(self):
        """ Renvoyer les départements non abonnés """
        return self.filter(instance__isnull=True)

    def by_cities(self, cities, as_initial=False):
        """
        Renvoyer les départements qui correspondent aux villes passées

        :param cities: liste ou queryset de villes, ou autorisation
        :param as_initial: renvoyer une liste des ID au lieu du queryset (pour Form.initial)
        :type as_initial: bool
        :returns: un queryset des départements, ou une liste des ID des départements
        """
        from agreements.models.mairie import MairieAvis
        from authorizations.models.autorisation import ManifestationAutorisation
        if isinstance(cities, (models.Manager, models.QuerySet)):
            cities = cities.all()
        if isinstance(cities, ManifestationAutorisation):
            communes = [avis.commune for avis in MairieAvis.objects.filter(authorization=cities)]
            departements = [commune.get_departement().id for commune in communes]
            departements = list(set(list(departements)))
            result = self.filter(id__in=departements)
        else:
            result = self.filter(arrondissement__commune__id__in=[c.id for c in cities])
        if as_initial:
            return list(result.values_list('id', flat=True))
        else:
            return result

    def get_by_name(self, name):
        """
        Renvoyer un département portant le nom passé en argument

        :return: une instance de département, ou None si non trouvé
        """
        try:
            return self.get(name__iexact=name)
        except Departement.DoesNotExist:
            return None


class Departement(models.Model):
    """ Division de niveau 2 (département) : total de 96+ en France métropolitaine """

    # Champs
    name = models.CharField("Département", unique=True, max_length=3, choices=DEPARTMENT_CHOICES)
    objects = DepartementManager()

    # Getter
    def get_instance(self):
        try:
            return self.instance
        except ObjectDoesNotExist:
            from core.models import Instance
            return Instance.objects.get_master()

    def get_name(self):
        """ Afficher le nom lisible du département """
        try:
            return self.get_name_display()
        except KeyError:
            return self.name

    # Overrides
    def __str__(self):
        return self.get_name_display()

    def natural_key(self):
        return self.name,

    # Méta
    class Meta:
        verbose_name = "Département"
        verbose_name_plural = "Départements"
        ordering = ['name']
        app_label = 'administrative_division'
        default_related_name = 'departements'
