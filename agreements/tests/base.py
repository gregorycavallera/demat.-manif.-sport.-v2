# coding: utf-8
from django.test import TestCase

from authorizations.factories import ManifestationAuthorizationFactory


class AvisTestsBase(TestCase):
    """ Mixin de tests des avis : jeu de données de base pour les tests d'avis """

    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.authorization = ManifestationAuthorizationFactory.create()
        self.manifestation = self.authorization.get_manifestation()
        self.commune = self.manifestation.departure_city
        self.departement = self.commune.get_departement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.activite = self.manifestation.activite
