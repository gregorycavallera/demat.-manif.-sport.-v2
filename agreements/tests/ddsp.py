# coding: utf-8

from django.urls import reverse

from agreements.tests.base import AvisTestsBase
from authorizations.models.autorisation import ManifestationAutorisation
from core.models.instance import Instance
from ..factories import *


class DDSPAvisTests(AvisTestsBase):
    """ Tests des avis DDSP """

    # Configuration
    def setUp(self):
        super().setUp()

    # Tests
    def test_get_absolute_url(self):
        """ Tester les URLs d'accès """
        avis = DDSPAvisFactory.create(authorization=self.authorization)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:ddsp_agreement_detail', kwargs={'pk': avis.pk}))

    def test_preavis(self):
        """ Tester les préavis """
        avis = DDSPAvisFactory.create(authorization=self.authorization)
        self.assertTrue(avis.are_preavis_validated())  # aucun préavis

    def test_workflow_ddsp(self):
        if isinstance(self.authorization, ManifestationAutorisation):
            self.instance.workflow_ddsp = Instance.WF_DDSP  # Utiliser un workflow DDSP
            self.instance.save()
            self.assertEqual(self.authorization.get_avis_count(), 1)  # Avis fédération (tjs)
            self.authorization.ddsp_concerned = True  # Créer automatiquement un avis DDSP
            self.authorization.save()
            self.assertEqual(self.authorization.get_avis_count(), 2)  # Avis fédération (tjs) + avis DDSP
