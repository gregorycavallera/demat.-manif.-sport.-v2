# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from core.util.permissions import require_role
from ..models import *


class Archives(ListView):
    """ Affichage de la liste d'avis des manifestations passées dans le dashboard """

    # Configuration
    model = Avis
    template_name_suffix = '_archives'

    @method_decorator(require_role('agent'))
    def dispatch(self, *args, **kwargs):
        return super(Archives, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        """ Selon le rôle de l'agent, renvoyer ce qu'il doit voir dans le dashboard """
        agent = self.request.user.agent
        instance = self.request.user.get_instance()
        user = self.request.user

        if user.has_role('federationagent'):
            return FederationAvis.objects.by_instance(request=self.request).finished().for_federation(agent.federationagent.federation)
        elif user.has_role('serviceagent'):
            return ServiceAvis.objects.finished().filter(service=agent.serviceagent.service)
        elif user.has_role('mairieagent'):
            return MairieAvis.objects.finished().filter(commune=agent.mairieagent.commune)
        elif user.has_role('edsragent'):
            if instance.get_workflow_ggd() == Instance.WF_EDSR:
                return EDSRAvis.objects.by_instance(request=self.request).finished()
            elif instance.get_workflow_ggd() == Instance.WF_GGD_EDSR:
                return GGDAvis.objects.by_instance(request=self.request).with_edsr().finished()
            else:
                return EDSRAvis.objects.none()
        elif user.has_role('sdisagent'):
            return SDISAvis.objects.by_instance(request=self.request).finished()
        elif user.has_role('ddspagent'):
            return DDSPAvis.objects.by_instance(request=self.request).finished()
        elif user.has_role('ggdagent'):
            if instance.get_workflow_ggd() == Instance.WF_EDSR:
                return EDSRAvis.objects.by_instance(request=self.request).finished().to_acknowledge()
            else:
                return GGDAvis.objects.by_instance(request=self.request).finished()
        elif user.has_role('cgagent'):
            return CGAvis.objects.by_instance(request=self.request).finished()
        elif user.has_role('cgsuperieuragent'):
            return CGAvis.objects.by_instance(request=self.request).finished().to_acknowledge()
        elif user.has_role('codisagent'):
            return SDISAvis.objects.by_instance(request=self.request).finished().acknowledged()
        elif user.has_role('cisagent'):
            return SDISAvis.objects.by_instance(request=self.request).finished().for_fire_service(agent.cisagent.cis).acknowledged()
        elif user.has_role('brigadeagent'):
            if instance.get_workflow_ggd() == Instance.WF_EDSR:
                return EDSRAvis.objects.by_instance(request=self.request).finished()
            elif instance.get_workflow_ggd() in [Instance.WF_GGD_EDSR, Instance.WF_GGD_SUBEDSR]:
                return GGDAvis.objects.by_instance(request=self.request).finished()
            else:
                return None

    def get_context_data(self, **kwargs):
        context = super(Archives, self).get_context_data(**kwargs)
        return context
