# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from administration.models.service import Commissariat
from agreements.views.base import BaseAcknowledgeView, BaseResendView, BaseDispatchView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class DDSPAvisDetail(DetailView):
    """ Vue de détail des avis DDSP """

    # Configuration
    model = DDSPAvis

    # Overrides
    @method_decorator(require_role('ddspagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents DDSP uniquement """
        return super(DDSPAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class DDSPAvisDispatch(BaseDispatchView):
    """ Vue d'envoi des demandes """

    # Confoguration
    model = DDSPAvis
    form_class = DDSPAvisDispatchForm

    # Overrides
    @method_decorator(require_role('ddspagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents DDSP unqiuement """
        return super(DDSPAvisDispatch, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Filtrer les services dispo à ceux des départements traversés
        form.fields['commissariats_concernes'].queryset = Commissariat.objects.filter(
            commune__arrondissement__departement__in=self.object.get_manifestation().get_crossed_departements())
        return form


class DDSPAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu d'avis DDSP """

    # Configuration
    model = DDSPAvis

    # Overrides
    @method_decorator(require_role('ddspagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue aux agents DDSP uniquement """
        return super(DDSPAvisAcknowledge, self).dispatch(*args, **kwargs)


class DDSPAvisResend(BaseResendView):
    """ Vue de renvoi des demandes d'avis """

    # Configuration
    model = DDSPAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.authorization.manifestation.departure_city.get_departement().ddsp.ddspagents.all())
        instance.log_resend(recipient=instance.get_ddsp().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
