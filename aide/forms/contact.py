# coding: utf-8
from ckeditor.widgets import CKEditorWidget
from crispy_forms.layout import Layout, Fieldset, Submit
from core.forms.base import GenericForm
from captcha.fields import CaptchaField

from aide.models import Demande


class DemandeForm(GenericForm):
    """ Formulaire de contact """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DemandeForm, self).__init__(*args, **kwargs)
        # Test si un utilisateur est connecté au chargement du formulaire et test si le captcha était requis lors de la
        # vérification du formulaire
        if 'email' in self.initial or (self.data.__len__() and not 'captcha_0' in self.data) :
            # pas de captcha
            self.helper.layout = Layout(Fieldset(*["Demande", 'type', 'contenu', 'departement', 'email']),
                                        Submit('submit', u'Envoyer', css_class='btn btn-primary'))
        else:
            # ajout du captcha
            self.fields['captcha'] = CaptchaField()
            self.helper.layout = Layout(Fieldset(*["Demande", 'type', 'departement', 'contenu', 'email', 'captcha']),
                                        Submit('submit', u'Envoyer', css_class='btn btn-primary'))
        self.helper.field_class = 'col-sm-8'

    class Meta:
        model = Demande
        exclude = ["status", "date", "user"]
        widgets = {'contenu': CKEditorWidget(config_name='minimal')}


class DemandeAdminForm(GenericForm):
    """ Gestion des demandes de contact """

    class Meta:
        model = Demande
        exclude = []
        widgets = {'contenu': CKEditorWidget(config_name='minimal')}
