# coding: utf-8
from ckeditor.widgets import CKEditorWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.forms.widgets import CheckboxSelectMultiple, Textarea

from aide.models.context import ContextHelp
from core.util.user import UserHelper


class ContextHelpForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = ContextHelp

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, widget=CheckboxSelectMultiple(), required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'role' in self.initial:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'text': CKEditorWidget(), 'page_names': Textarea, 'positions': Textarea}
