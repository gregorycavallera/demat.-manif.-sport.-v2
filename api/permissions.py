from rest_framework import permissions


class IsInGroupApi(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):

        grouplist = request.user.groups.values_list('name', flat=True)
        if 'API' in grouplist:
            return True
        return False
