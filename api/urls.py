from django.urls import include, path
from api import views as api_views
from rest_framework import routers


app_name = 'api'
router = routers.DefaultRouter()
# router.register(r'manifestations', api_views.ManifestationAllViewSet)
router.register(r'ManifestationAutorisation', api_views.ManifestationAutorisationViewSet, base_name='ManifestationAutorisation')
router.register(r'ManifestationDeclaration', api_views.ManifestationDeclarationViewSet, base_name='ManifestationDeclaration')
router.register(r'Organisateur', api_views.OrganisateurViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
