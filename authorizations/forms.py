# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, Fieldset
from django import forms
from django.urls import reverse_lazy
from django.forms.models import ModelMultipleChoiceField

from administration.models.service import Service
from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelMultipleChoiceField
from clever_selects.forms import ChainedChoicesModelForm
from core.forms.base import GenericForm
from .models import ManifestationAutorisation


class AuthorizationForm(GenericForm):
    """ Formulaire de demande d'autorisation """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(AuthorizationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(FormActions(Submit('save', "Demander une autorisation")))

    # Méta
    class Meta:
        model = ManifestationAutorisation
        exclude = ['manifestation', 'creation_date', 'dispatch_date', 'state', 'departements', 'concerned_cities', 'concerned_services', 'ddsp_concerned',
                   'edsr_concerned', 'ggd_concerned', 'sdis_concerned', 'cg_concerned', 'bylaw']


class AuthorizationDispatchForm(GenericForm, ChainedChoicesModelForm):
    """ Formulaire de distribution des demandes d'autorisation """

    # Champs
    concerned_departments = ModelMultipleChoiceField(required=False, queryset=Departement.objects.configured(), label="Départements concernés")
    concerned_cities = ChainedModelMultipleChoiceField('concerned_departments', reverse_lazy('administrative_division:commune_widget'),
                                                       Commune, label="Mairies concernées", required=False)
    concerned_services = ChainedModelMultipleChoiceField('concerned_departments', reverse_lazy('administration:service_widget'),
                                                       Service, label="Services concernés", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super(AuthorizationDispatchForm, self).__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Fieldset("Avis requis",
                     'ddsp_concerned', 'edsr_concerned', 'ggd_concerned', 'sdis_concerned', 'cg_concerned', 'concerned_departments', 'concerned_cities',
                     'concerned_services'),
            FormActions(Submit('save', "Distribuer"))
        )
        self.fields['concerned_departments'].widget.attrs = {'data-placeholder': "Sélectionnez un ou plusieurs départements"}
        self.fields['concerned_cities'].widget.attrs = {'data-placeholder': "Sélectionnez une ou plusieurs communes"}
        self.fields['concerned_services'].widget.attrs = {'data-placeholder': "Sélectionnez un ou plusieurs services"}

    # Méta
    class Meta:
        model = ManifestationAutorisation
        exclude = ['manifestation', 'creation_date', 'dispatch_date', 'state', 'bylaw']


class AuthorizationPublishBylawForm(GenericForm):
    """ Formulaire de publication d'arrêté """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(AuthorizationPublishBylawForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Choisissez le fichier de l'arrêté", 'bylaw'),
            FormActions(Submit('save', "Publier l'arrêté"))
        )

    # Validation des champs
    def clean_bylaw(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['bylaw']
        if not data:
            raise forms.ValidationError("Vous devez spécifier un fichier contenant l'arrêté")
        return data

    # Meta
    class Meta:
        model = ManifestationAutorisation
        fields = ['bylaw']
