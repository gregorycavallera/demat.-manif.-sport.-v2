                                                                                                                                # coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from authorizations.models import ManifestationAutorisation
from authorizations.signals import authorization_published
from sub_agreements.models.cgd import PreAvisCGD
from sub_agreements.models.commissariat import PreAvisCommissariat
from sub_agreements.models.compagnie import PreAvisCompagnie
from sub_agreements.models.servicecg import PreAvisServiceCG


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=ManifestationAutorisation)
    def log_authorization_creation(created, instance, **_):
        """ Enregistrer la création d'une nouvelle autorisation """
        if created:
            instance.log_creation()
            instance.notify_creation()


    @receiver(authorization_published, sender=ManifestationAutorisation)
    def notify_authorization_publication(instance, **_):
        """ Notifier tous les agents locaux de la publication de l'arrêté """
        for preavis in PreAvisCGD.objects.filter(avis__authorization=instance):
            preavis.notify_publication(agents=preavis.get_agents())
        for preavis in PreAvisCompagnie.objects.filter(avis__authorization=instance):
            preavis.notify_publication(agents=preavis.get_agents())
        for preavis in PreAvisCommissariat.objects.filter(avis__authorization=instance):
            preavis.notify_publication(agents=preavis.get_agents())
        for preavis in PreAvisServiceCG.objects.filter(avis__authorization=instance):
            preavis.notify_publication(agents=preavis.get_agents())
