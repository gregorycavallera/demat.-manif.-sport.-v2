# coding: utf-8
from django.core.management.base import BaseCommand

from carto.consumer.openrunner import openrunner_api
from core.models.user import User
from core.util.security import protect_code


class Command(BaseCommand):
    """ Créer les utilisateurs Openrunner via l'API """
    args = ''
    help = "Créer les utilisateurs Openrunner des agents SDIS (security_officer) et des instructeurs (instructor)"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        created = 0
        total = 0

        for user in User.objects.agents('sdisagent'):
            success = openrunner_api.create_user(user.get_username(), user.get_instance().get_departement_name(), 'security_officer')
            total += 1
            if success is True:
                created += 1

        for user in User.objects.by_role('instructeur'):
            success = openrunner_api.create_user(user.get_username(), user.get_instance().get_departement_name(), 'instructor')
            total += 1
            if success is True:
                created += 1

        print("{count} nouveaux utilisateurs Openrunner ont été créés sur un total de {total}".format(count=created, total=total))
