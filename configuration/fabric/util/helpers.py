# coding: utf-8
from functools import wraps
from time import time

from fabric import api


def time_it(func):
    """ Décorateur affichant le temps d'exécution d'une fonction """

    @wraps(func)
    def timed(*args, **kwargs):
        print("timing execution of {name} in machine named {host}".format(name=func.__name__, host=api.env.host))

        start = time()
        result = func(*args, **kwargs)
        elapsed = time() - start

        print("operation {name} was executed in {elapsed:.2f} seconds.".format(elapsed=elapsed, name=func.__name__))
        print("\a")
        return result

    return timed


def read_requirements(filename):
    """ Lire un fichier texte de prérequis et renvoyer une liste """
    with open('./fabric_setup/configuration/{name}'.format(name=filename), 'r') as f:
        lines = [line for line in f.readlines() if line.strip() and not line.startswith('[')]
        lines = [line.rstrip('\r\n') for line in lines]
        return lines


def account_password(default_pwd, version=None):
    """
    Lire ./configuration/account-password.txt et renvoyer la première ligne
    :param default_pwd: mot de passe par défaut
    :param version: variation sur le nom de fichier
    """
    version = '-{0}'.format(version) if version else version
    try:
        with open('./configuration/account-password{version}.txt'.format(version=version), 'r') as f:
            lines = f.readlines()
            pwd = lines[0].strip().replace('\n', '')
            if pwd:
                return pwd
            elif default_pwd:
                return default_pwd
            else:
                raise ValueError('default password or password file should be provided.')
    except IOError:
        if default_pwd:
            return default_pwd
        else:
            raise ValueError('default password or password file should be provided.')
