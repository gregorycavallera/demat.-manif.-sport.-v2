# coding: utf-8
from fabric.api import env, task


@task
def preproduction():
    """ Définir les informations de préprod """
    env.server = 'preproduction'
    env.hostname = 'HOSTNAME'
    env.hosts = [''.join([OWNER, '@', env.hostname])]
    env.db_name = 'db_name'
    env.db_user = 'db_user'
    env.db_password = 'db_password'


@task
def production():
    """ Définir les informations de production """
    env.server = 'production'
    env.hostname = 'HOSTNAME'
    env.hosts = [''.join([OWNER, '@', env.hostname])]
    env.db_name = 'db_name'
    env.db_user = 'db_user'
    env.db_password = 'db_password'
