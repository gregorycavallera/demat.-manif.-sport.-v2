# coding: utf-8
import logging
import sys
from collections import OrderedDict

from core.util.migrations import DisableMigrations
from .directory import Paths
from . import thirdparty


# DJANGO BASE SETTINGS
# =========================
# Sécurité
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'  # à remplacer en production
ALLOWED_HOSTS = ['.localhost', '.manifestationsportive.fr']

# Debug
DEBUG = True

# WSGI et routage
ROOT_URLCONF = 'configuration.settings.urls.root'
WSGI_APPLICATION = 'configuration.wsgi.base.application'

# Traduction et Régionalisation (https://docs.djangoproject.com/en/1.6/topics/i18n/)
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'  # bootstrap3_datetime nécessite un code en 2 lettres
USE_I18N = True
USE_L10N = True
USE_TZ = True
DATETIME_INPUT_FORMATS = ['%d/%m/%Y %H:%M', '%d/%m/%Y %H:%M:%s']
DATE_INPUT_FORMATS = ['%d/%m/%Y']
DATE_FORMAT = "d/m/Y"
DATETIME_FORMAT = "d F Y H:i"

# Framework Sites (https://docs.djangoproject.com/en/1.6/ref/contrib/sites/)
SITE_ID = 1

# Fichiers statiques et fichiers uploadés (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
# https://docs.djangoproject.com/en/1.6/topics/files/
STATIC_URL = '/static/'
STATICFILES_DIRS = []
STATIC_ROOT = '/home/.../static'  # Utiliser Paths.get_root_dir pour un répertoire relatif à celui du projet
MEDIA_ROOT = '/home/.../media/'
MEDIA_URL = '/media/'
# Limite mémoire d'upload de fichier dans Django (mais pas dans Apache/Nginx)
FILE_UPLOAD_MAX_MEMORY_SIZE = 10485760

# Email backend (https://docs.djangoproject.com/en/1.6/topics/email/#email-backends)
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = 'Manifestationsportive.fr <plateforme@manifestationsportive.fr>'

# DATABASE SETTINGS
# =================
# Base de données principale (https://docs.djangoproject.com/en/1.6/ref/settings/#databases)
DATABASES = OrderedDict([
    ['default', {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'main',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': 5432,
        'NUMBER': 0
    }],
    ['import_42', {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'import_42',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': 5432,
        'NUMBER': 42
    }],
    ['import_78', {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'import_78',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': 5432,
        'NUMBER': 78
    }],
    ['import_972', {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'import_972',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': 5432,
        'NUMBER': 972
    }]
])

# CACHE SETTINGS
# ==============
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# DJANGO USER AND AUTH SETTINGS
# =============================
# Cookie
SESSION_COOKIE_DOMAIN=".manifestationsportive.fr"

# Modèle utilisateur utilisé par Django (1.5+)
AUTH_USER_MODEL = 'core.user'

# Authentification (https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends)
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Password hashers (https://docs.djangoproject.com/en/1.6/ref/settings/#password-hashers)
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'core.util.hashers.DrupalPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

# TEMPLATES & MIDDLEWARE
# ======================
# Configuration du moteur de templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            Paths.get_root_dir('portail', 'templates'),
            Paths.get_root_dir('templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'core.processors.core.core',
                'aide.processors.contexthelp.contexthelps'
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'debug': DEBUG,
        },
    },
]

# Middleware
MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware'
)

# APPLICATIONS
# ============
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'django_jenkins',
    'adminsortable2',
    'ajax_select',
    'localflavor',
    'allauth',
    'allauth.account',
    'captcha',
    'bootstrap3_datetime',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'debug_toolbar',
    'import_export',
    'fixture_magic',
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'contacts',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'protected_areas',
    'sports',
    'sub_agreements',
    'nouveautes',
    'core',
    'aide',
    'carto',
    'organisateurs',
    'django_custom_user_migration',
    'legacy',
    'clever_selects',
    'django_filters',
    'rest_framework_swagger',
    'rest_framework',
)

# LOGGING
# ==================================
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'carto_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/manifsport/carto.debug.log',
            'formatter': 'verbose'
        },
        'smtp_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/manifsport/smtp.debug.log',
            'formatter': 'verbose'
        },
        'api_file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': '/var/log/manifsport/api.info.log',
            'formatter': 'simple'
        },
    },
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s (pid %(process)d) %(message)s'},
        'simple': {'format': '%(levelname)s | %(asctime)s | %(message)s'},
    },
    'loggers': {
        'carto': {
            'handlers': ['carto_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'smtp': {
            'handlers': ['smtp_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'api': {
            'handlers': ['api_file'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

# Configuration de l'API
#=======================
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
            'rest_framework.renderers.BrowsableAPIRenderer',
        ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        ),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated', ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend', ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30
}

# APPLICATION MANIFESTATION SPORTIVE
# ==================================
# URL principale du site.
MAIN_URL = 'http://manifsportpreprod.openscop.info/'

# Informations de version HEAD
VERSION = {
    'VERSION': '2.80',
    'DATE': '12/11/2016',
    'COMMIT': 'f517c500af35de771b5d3c9df7746e1c717dcfee'
}

# Paramètre de maintenance : désactiver la création auto d'avis etc.
DISABLE_SIGNALS = False

# Alias de formulaire pour la validation auto
FORM_ALIASES = {
    'manifestation': ['events.forms.ManifestationForm'],
    'manifestation_autorisationnm': ['events.forms.AutorisationNMForm'],
    'manifestation_declarationnm': ['events.forms.DeclarationNMForm'],
    'manifestation_motorizedconcentration': ['events.forms.MotorizedConcentrationForm'],
    'manifestation_motorizedevent': ['events.forms.MotorizedEventForm'],
    'manifestation_motorizedrace': ['events.forms.MotorizedRaceForm'],
    'umanifestation': ['events.forms.UnsupportedManifestationForm']
}

# TESTS
# =====
TESTS_IN_PROGRESS = False
if 'test' in sys.argv[1:] or 'jenkins' in sys.argv[1:]:
    logging.disable(logging.CRITICAL)
    DEBUG = False
    TEMPLATE_DEBUG = False
    TESTS_IN_PROGRESS = True
    MIGRATION_MODULES = DisableMigrations()
    DATABASES = {k: v for k, v in DATABASES.items() if k is 'default'}
    DISABLE_SIGNALS = False

# FUSION des bases / LEGACY
# =========================
if 'full_merge' in sys.argv[1:] or 'import_external' in sys.argv[1:]:
    DISABLE_SIGNALS = True  # Ne surtout pas conserver le comportement des signaux lors de l'importation

# PROTECTION de base du code sensible
# ===================================
PROTECT_CODE = False  # Certaines commandes ne peuvent pas s'exécuter si == True

# SETTINGS RUNSERVER
# ==================
for arg in sys.argv[1:]:
    if 'runserver' in arg or 'unicorn' in arg:
        DISABLE_SIGNALS = False

# Empêcher l'effacement de .thirdparty lors du nettoyage automatique des imports
for key in thirdparty.__dict__:
    if key == key.upper():
        locals()[key] = getattr(thirdparty, key)

# SENTRY
# ======
RAVEN_CONFIG = {'dsn': ''}

# django-simple-captcha
# =====================
CAPTCHA_FOREGROUND_COLOR = '#013A50'
CAPTCHA_BACKGROUND_COLOR = '#BAC420'
CAPTCHA_FONT_SIZE = 28
CAPTCHA_LENGTH = 6
