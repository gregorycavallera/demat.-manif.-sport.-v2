# coding: utf-8

# Openrunner settings
OPENRUNNER_HOST = 'http://ddcs.openrunner.com/'

OPENRUNNER_ROUTE_DISPLAY = 'orservice/serviceDDCS.php?idr='
OPENRUNNER_MULTI_ROUTES_DISPLAY = 'orservice/serviceMPDDCS.php?idr='

# Données pour l'API OpenRunner
OPENRUNNER_API_URL = 'https://ddcs2.openrunner.com/api/'
OPENRUNNER_FRONTEND_URL = 'http://ddcs.openrunner.com'
OPENRUNNER_TOKEN = 'api token used for openrunner'
