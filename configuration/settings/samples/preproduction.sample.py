# coding: utf-8
"""
Django Pre-production settings for Manifestation Sportive project.
"""

from .base import *
from .directory import Paths
from os import path

# Paramètres différents de ceux de base
# =====================================
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'  # Sample
DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = ['']

# DATABASE SETTINGS
# =================
# Base de données principale (https://docs.djangoproject.com/en/1.6/ref/settings/#databases)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'main',
        'USER': 'user',
        'PASSWORD': 'password'
    }
}

STATIC_ROOT = '/home/dev/3000/releases/current/static/'
RAVEN_CONFIG = {'dsn': ''}
