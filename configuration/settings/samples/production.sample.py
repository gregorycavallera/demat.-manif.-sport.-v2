# coding: utf-8
"""
Django Production settings for ddcs_loire project.
"""

from .base import *
from .directory import Paths
import os
import raven

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'

# Needed for SSL
# See https://docs.djangoproject.com/en/1.6/ref/settings/#std:setting-SESSION_COOKIE_SECURE
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['.manifestationsportive.fr']

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'raven.contrib.django.raven_compat',
    'localflavor',
    'adminsortable2',
    'allauth',
    'allauth.account',
    'captcha',
    'bootstrap3_datetime',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'import_export',
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'contacts',
    'core',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'nouveautes',
    'protected_areas',
    'sports',
    'sub_agreements',
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': 'localhost',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
    }
}

STATIC_ROOT = '/home/rtadmin/3000/releases/current/static/'

MEDIA_ROOT = '/var/www/3000/media/'


# Email backend
# https://docs.djangoproject.com/en/1.6/topics/email/#email-backends
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True


# Exceptions logging :
# https://sentry.io/Openscop/prodmanifsport3/
RAVEN_CONFIG = {
    'dsn': '',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    #'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),
}

# django-simple-captcha
# =====================
CAPTCHA_FOREGROUND_COLOR = '#013A50'
CAPTCHA_BACKGROUND_COLOR = '#BAC420'
CAPTCHA_FONT_SIZE = 28
CAPTCHA_LENGTH = 6