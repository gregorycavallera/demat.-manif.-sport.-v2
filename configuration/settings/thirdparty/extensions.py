# coding: utf-8
""" Settings Django-extensions """

# EXTENSIONS
# Reset DB
POSTGIS_TEMPLATE = None
# Shell Plus
IPYTHON_ARGUMENTS = ['--ext', 'django_extensions.management.notebook_extension', '--debug']
SHELL_PLUS_DONT_LOAD = []
SHELL_PLUS_POST_IMPORTS = (
    ('base64', '*'),
    ('collections', '*'),
    ('socket', ('gethostname',)),
    ('datetime', ('datetime', 'timedelta')),
    ('django.db.models', '*'),
    ('django.utils.translation', 'activate'),
    ('math', '*'),
    ('core.util', ('application', 'diagnose')),
    ('core.util.user', ('UserHelper',)),
    ('carto.consumer.openrunner', ('openrunner_api', 'OpenRunnerAPI')),
    'requests'
)
