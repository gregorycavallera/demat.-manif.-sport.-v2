import debug_toolbar
from django.conf import settings
from django.urls import include, path, re_path
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.flatpages import views as flatpages_views
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.flatpages.sitemaps import FlatPageSitemap

from core.views.ajax import validate_form
from core.views.user import ProfileDetailView, UserUpdateView, signupagentview, signuporganisateurview, EmailConfirmedView
from organisateurs.views import StructureUpdateView
from portail.views.index import HomePage

# Todo : à supprimer après réécriture de la documentation (renvoi de l'ancienne doc vers aide/help_page)
from aide import views

admin.autodiscover()

sitemaps = {
    'flatpages': FlatPageSitemap,
}
handler403 = 'portail.views.forbidden_view'

urlpatterns = [
    # Examples:
    # path('', 'ddcs_loire.views.home', name='home'),
    # path('blog/', include('blog.urls')),

    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('sitemap\.xml', sitemaps_views.sitemap, {'sitemaps': sitemaps}),
    path('accounts/', include('allauth.urls')),
    path('accounts/email_confirmed', EmailConfirmedView.as_view(), name='email_confirmed'),
    path('accounts/profile/', ProfileDetailView.as_view(), name='profile'),
    path('accounts/profile/edit', UserUpdateView.as_view(), name='profile_edit'),
    path('accounts/profile/structure/edit', StructureUpdateView.as_view(), name='structure_edit'),
    path('inscription/agent', signupagentview, name='inscription_agent'),
    path('inscription/organisateur', signuporganisateurview, name='inscription_organisateur'),
    path('administration/', include('administration.urls')),
    path('agreements/', include('agreements.urls')),
    path('carto/', include('carto.urls')),
    path('sub_agreements/', include('sub_agreements.urls')),
    path('authorizations/', include('authorizations.urls')),
    path('declarations/', include('declarations.urls')),
    path('evaluations/', include('evaluations.urls')),
    path('nouveautes/', include('nouveautes.urls')),
    path('sports/', include('sports.urls')),
    path('administrative_division/', include('administrative_division.urls')),
    path('', include('events.urls')),
    path('protected_areas/', include('protected_areas.urls')),
    re_path('validate/(?P<alias>[\w\-]+)/', validate_form, name='validate'),
    path('ajax_select/', include('ajax_select.urls')),
    path('', include('portail.urls')),
    path('', HomePage.as_view(), name='home_page'),
    path('aide/', include('aide.urls')),
    path('api/', include('api.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('captcha/', include('captcha.urls')),
    # Todo : à réactiver après réécriture de la documentation (renvoi de l'ancienne doc vers aide/help_page)
    # path('(?P<url>.*/)', flatpages_views.flatpage),
    # Todo : à supprimer après réécriture de la documentation (renvoi de l'ancienne doc vers aide/help_page)
    re_path('(?P<path>.+)', views.view_help_page, name="help-page"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns
