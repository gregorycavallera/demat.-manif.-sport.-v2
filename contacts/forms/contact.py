# coding: utf-8
from django import forms
from extra_views.generic import GenericInlineFormSet
from localflavor.fr.forms import FRPhoneNumberField

from core.forms.base import GenericForm
from ..models import Contact


class ContactForm(GenericForm):
    """ Formulaire """

    # Champs
    phone = FRPhoneNumberField()

    # Overrides
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = FRPhoneNumberField()
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = Contact
        exclude = ('content_type', 'object_id', 'content_object')


class ContactInline(GenericInlineFormSet):
    """ Formset Inline des conteacts """

    # Configuration
    model = Contact
    fields = ['first_name', 'last_name', 'phone']
    extra = 1
    max_num = 1
    can_delete = False
    form_class = ContactForm
