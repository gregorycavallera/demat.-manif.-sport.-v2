# coding: utf-8
from crispy_forms.helper import FormHelper
from django import forms


class GenericForm(forms.ModelForm):
    """ Formulaire de base """

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super(GenericForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
