# coding: utf-8
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from django import forms
from allauth.account.forms import SignupForm
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from django.apps import apps
from localflavor.fr.forms import FRPhoneNumberField
from django.core.mail import send_mail

from administrative_division.models import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField
from clever_selects.forms import ChainedChoicesForm
from core.forms.base import GenericForm
from core.models.instance import Instance
from organisateurs.models import Organisateur, Structure, StructureType
from administration.models.service import *
from administration.models import Agent, AgentLocal, Observateur
from sports.models import Federation
from emergencies.models import Association1ersSecours


class UserUpdateForm(GenericForm):
    """ Formulaire de modification de l'utiliasteur """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Enregistrer"))

    # Meta
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'default_instance')


class CustomUserChangeForm(UserChangeForm):

    def clean(self):
        cleaned_data = super(CustomUserChangeForm, self).clean()
        if cleaned_data['is_active'] != self.initial['is_active']:
            if cleaned_data['is_active'] == True:
                send_mail(
                    'Validation d\'inscription',
                    "Mr ou Mme " + self.instance.first_name + '  ' + self.instance.last_name + chr(10) +
                    'Votre compte est maintenant validé sur la plateforme ' + Site.objects.get_current().domain + '.' + chr(10) +
                    "nom d'utilisateur : " + self.instance.username,
                    'plateforme@manifestationsportive.fr',
                    [self.instance.email],
                )


class SignupAgentForm(SignupForm):
    """ Formulaire d'inscription des agents des services"""
    # Liste de sélection des divers services pour un agent
    SERVICE_LISTE = (
        ('', '---------'),
        ('Prefecture', 'Préfecture'),
        ('Commune', 'Mairie'),
        ('Federation', 'Fédération sportive'),
        ('CG', 'Conseil Départemental'),
        ('CGn+1', 'Conseil Départemental n+1'),
        ('CG_Service', 'Services du Conseil Départemental'),
        ('GGD', 'Groupement de Gendarmerie Départemental'),
        ('CGD', 'Compagnie de Gendarmerie Départementale'),
        ('EDSR', 'Escadron Départemental de Sécurité Routière'),
        ('Brigade', 'Brigade EDSR'),
        ('DDSP', 'Direction Départementale de Sécurité Publique'),
        ('Commissariat', 'Commissariat de Police'),
        ('SDIS', 'Service Départemental Incendie et Secours'),
        ('CODIS', 'Center d\'Opérations Départemental Incendie et Secours'),
        ('Compagnie', 'Compagnie SDIS'),
        ('CIS', 'Caserne / Service feu'),
        ('Association1ersSecours', 'Associations de premiers secours'),
        ('Service', 'Autre Service'),
    )
    # Liste des sous sélections des services multiples d'une instance
    SERVICE_FIELD = {
        'Prefecture': 'prefecture',
        'Commune': 'commune',
        'Federation': 'federation',
        'CG_Service': 'cgservice',
        'CGD': 'cgd',
        'Brigade': 'brigade',
        'Commissariat': 'commissariat',
        'Compagnie': 'compagnie',
        'CIS': 'cis',
        'Association1ersSecours': 'secours',
        'Service': 'autre',
    }

    def get_from_serviceliste(self, param):
        # Retrouver le titre détaillé en fonction de la sélection
        titre = ''
        for couple in self.SERVICE_LISTE:
            if param in couple:
                titre = couple[1]
        return titre

    # Champs
    first_name = forms.CharField(max_length=30, label="Prénom du déclarant")
    last_name = forms.CharField(max_length=30, label="Nom du déclarant")

    instance = ModelChoiceField(queryset=Instance.objects.configured(), label="Département")
    service = forms.ChoiceField(choices=SERVICE_LISTE)

    # Boutons de sélection optionnels
    prefecture = ChainedModelChoiceField('instance', reverse_lazy('administration:prefecture_widget'), Prefecture, label="Préfecture", required=False)
    commune = ChainedModelChoiceField('instance', reverse_lazy('administration:communeinstance_widget'), Commune, label="Commune", required=False)
    federation = ChainedModelChoiceField('instance', reverse_lazy('administration:federationinstance_widget'), Federation, label="Fédération", required=False)
    cgservice = ChainedModelChoiceField('instance', reverse_lazy('administration:cgservice_widget'), CGService, label="Service du Conseil Départemental", required=False)
    cgd = ChainedModelChoiceField('instance', reverse_lazy('administration:cgd_widget'), CGD, label="Compagnie de Gendarmerie Départemental", required=False)
    brigade = ChainedModelChoiceField('instance', reverse_lazy('administration:brigade_widget'), Brigade, label="Brigade EDSR", required=False)
    commissariat= ChainedModelChoiceField('instance', reverse_lazy('administration:commissariat_widget'), Commissariat, label="Commissariat de Police", required=False)
    compagnie= ChainedModelChoiceField('instance', reverse_lazy('administration:compagnie_widget'), Compagnie, label="Compagnie SDIS", required=False)
    cis= ChainedModelChoiceField('instance', reverse_lazy('administration:cis_widget'), CIS, label="Caserne / Service feu", required=False)
    secours= ChainedModelChoiceField('instance', reverse_lazy('administration:secours_widget'), Association1ersSecours, label="Associations de premiers secours", required=False)
    autre= ChainedModelChoiceField('instance', reverse_lazy('administration:autreservice_widget'), Service, label="Autre Service", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        """ Initialiser l'objet """
        super(SignupAgentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['username'].help_text = "Le nom d'utilisateur vous servira comme identifiant lors de la connexion au site"
        self.fields['username'].label = "Nom d'utilisateur"
        self.helper.layout = Layout(
            Fieldset(
                "Création d'un compte Agent",
                HTML("<div class='well'>{text}</div>".format(
                    text="Veuillez renseigner votre identité en tant qu'agent instructeur ou en tant qu'agent d'un service consulté."
                )),
                'first_name',
                'last_name',
                'username',
                'email',
                'password1',
                'password2',
            ),
            Fieldset(
                "Rattachement à un service",
                HTML("<div class='well'>{text}</div>".format(
                    text="Veuillez sélectionner le service pour lequel vous officiez. "
                )),
                'instance',
                'service',
                'prefecture',
                'commune',
                'federation',
                'cgservice',
                'cgd',
                'brigade',
                'commissariat',
                'compagnie',
                'cis',
                'secours',
                'autre',
            )

        )
        self.helper.add_input(Submit('submit', "Inscription"))

    def clean(self):
        # Vérifier que le champ optionnel soit rempli
        cleaned_data = super().clean()
        service = cleaned_data.get('service')
        if service in self.SERVICE_FIELD:
            if not cleaned_data.get(self.SERVICE_FIELD[service]):
                self.add_error(self.SERVICE_FIELD[service], "Ce champ est obligatoire")
                raise forms.ValidationError("Une valeur doit être sélectionnée dans le champ " + self.get_from_serviceliste(service))
        return cleaned_data

    def save(self, request):
        # Liste de correspondance entre la sélection du service et l'agent associé
        SERVICE_MODEL = {
            'Prefecture': 'Instructeur',
            'Commune': 'MairieAgent',
            'Federation': 'FederationAgent',
            'CG': 'CGAgent',
            'CGn+1': 'CGSuperieur',
            'CG_Service': 'CGServiceAgentLocal',
            'GGD': 'GGDAgent',
            'CGD': 'CGDAgentLocal',
            'EDSR': 'EDSRAgent',
            'Brigade': 'BrigadeAgent',
            'DDSP': 'DDSPAgent',
            'Commissariat': 'CommissariatAgentLocal',
            'SDIS': 'SDISAgent',
            'CODIS': 'CODISAgent',
            'Compagnie': 'CompagnieAgentLocal',
            'CIS': 'CISAgent',
            'Association1ersSecours': 'Secouriste',
            'Service': 'ServiceAgent',

        }
        user = super(SignupAgentForm, self).save(request)
        user.is_active = False
        instance = self.cleaned_data['instance']
        user.default_instance = instance
        user.save()
        # La confirmation de l'adresse mail se fait si is_active est True
        user.is_active = True

        # Service sélectionné et nom du modèle d'agent
        servicevalue = self.cleaned_data['service']
        agentvalue = SERVICE_MODEL[servicevalue]
        # Nom de la clé du dictionnaire pour la création de l'objet agent
        agentfieldvalue = 'agent'
        # Cas particulier de l'EDSR suivant le workflow GGD
        if servicevalue == 'EDSR' and instance.workflow_ggd == instance.WF_GGD_SUBEDSR:
            agentvalue = 'EDSRAgentLocal'
            agentfieldvalue = 'agentlocal'
        # Récupération de l'objet modèle de l'agent
        agentmodel = apps.get_model(app_label='administration', model_name=agentvalue)
        # Cas particulier du CG Supérieur, le service reste CG
        if servicevalue == 'CGn+1':
            servicevalue = 'CG'
        # Création de objet générique agent et récupération de l'objet service
        servicefieldvalue = servicevalue.lower()
        if servicevalue not in self.SERVICE_FIELD:
            servicemodel = apps.get_model(app_label='administration', model_name=servicevalue)
            serviceobject = servicemodel.objects.get(departement=instance.departement)
            if 'Local' in agentvalue:
                agent = AgentLocal.objects.create(user=user)
            else:
                agent = Agent.objects.create(user=user)
        else:
            serviceobject = self.cleaned_data[self.SERVICE_FIELD[servicevalue]]
            if 'Local' in agentvalue:
                agentfieldvalue = 'agentlocal'
                agent = AgentLocal.objects.create(user=user)
            elif 'Secouriste' in agentvalue:
                agentfieldvalue = 'observateur_ptr'
                servicefieldvalue = 'association'
                agent = Observateur.objects.create(user=user)
            elif 'Instructeur' not in agentvalue:
                agent = Agent.objects.create(user=user)
        # Création de l'objet agent
        if 'Instructeur' in agentvalue:
            data_dict = {
                servicefieldvalue: serviceobject,
                'user': user,
            }
        else:
            data_dict = {
                servicefieldvalue: serviceobject,
                agentfieldvalue: agent,
                'user': user,
            }
        agentmodel.objects.create(**data_dict)
        return user


class SignupOrganisateurForm(SignupForm):
    """ Formulaire d'inscription des organisateurs"""

    # Champs
    first_name = forms.CharField(max_length=30, label="Prénom du déclarant")
    last_name = forms.CharField(max_length=30, label="Nom du déclarant")
    cgu = forms.BooleanField(label="En cochant cette case, j'accepte les <a href='/aide/CGU'>Conditions Générales d'Utilisation</a>")

    # Structure
    structure_name = forms.CharField(
        label="Nom de la structure", max_length=200,
        help_text="Précisez le nom de la structure organisatrice (nom de l'association ou du club, de la société ou de la personne physique s'il s'agit d'un "
                  "particulier) de la manifestation"
    )
    type_of_structure = forms.ModelChoiceField(
        label="Forme juridique de la structure", queryset=StructureType.objects.all(),
        help_text="Opérez votre choix parmi les statuts juridiques proposés"
    )
    address = forms.CharField(label="Adresse", max_length=255)
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Commune")
    phone = forms.CharField(label="Numéro de téléphone", max_length=14)
    website = forms.URLField(label="Site web", max_length=200, required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        """ Initialiser l'objet """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['phone'] = FRPhoneNumberField()
        self.fields['username'].help_text = "le nom d'utilisateur vous servira comme identifiant lors de la connexion au site"
        self.fields['username'].label = "Nom d'utilisateur"
        self.helper.layout = Layout(
            Fieldset(
                "Organisateur",
                HTML("<div class='well'>{text}</div>".format(
                    text="Veuillez renseigner votre identité en tant que représentant de la structure au "
                         "compte de laquelle vous effectuez la déclaration ou la demande d'autorisation."
                )),
                'first_name',
                'last_name',
                'username',
                'email',
                'password1',
                'password2',
                'cgu',
            ),
            Fieldset(
                "Structure",
                'structure_name',
                'type_of_structure',
                'address',
                'departement',
                'commune',
                'phone',
                'website',
            )
        )
        self.helper.add_input(Submit('submit', "Inscription"))

    def clean_structure_name(self):
        structure_name = self.cleaned_data['structure_name']
        if Structure.objects.filter(name=structure_name).exists():
            raise forms.ValidationError("Le nom \"{name}\" est déjà utilisé.".format(name=structure_name))
        return structure_name

    def save(self, request):
        user = super(SignupOrganisateurForm, self).save(request)
        cgu = self.cleaned_data['cgu']
        organisateur = Organisateur(user=user, cgu=cgu)
        organisateur.save()
        structure = Structure.objects.create(
            name=self.cleaned_data['structure_name'],
            type_of_structure=self.cleaned_data['type_of_structure'],
            phone=self.cleaned_data['phone'],
            website=self.cleaned_data['website'],
            organisateur=organisateur,
            address=self.cleaned_data['address'],
            commune=self.cleaned_data['commune'],
        )
        user.default_instance = structure.commune.get_instance()
        user.save()
        return user
