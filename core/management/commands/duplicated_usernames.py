# coding: utf-8
from django.contrib.auth import get_user_model
from django.core.management.base import NoArgsCommand


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        for user in get_user_model().objects.all():
            if get_user_model().objects.filter(username=user.username).count() > 1:
                print("{} - {}".format(str(user), user.email))
