# coding: utf-8
import re

from allauth.account.adapter import DefaultAccountAdapter
from django import forms
from django.urls import reverse

from core.util.user import UserHelper


class ManifsportAccountAdapter(DefaultAccountAdapter):
    """ Account adapter (Django Allauth) """

    def get_login_redirect_url(self, request):
        """ Renvoyer l'URL de redirection après connexion """

        # Utiliser le mécanisme par défaut pour la validation mais ne pas renvoyer l'URL
        base_home = super().get_login_redirect_url(request)
        user = request.user
        if UserHelper.has_role(user, 'instructeur'):
            return reverse('authorizations:dashboard')
        elif UserHelper.has_role(user, 'organisateur'):
            return reverse('events:dashboard')
        elif UserHelper.has_role(user, 'agent'):
            return reverse('agreements:dashboard')
        elif UserHelper.has_role(user, 'agentlocal'):
            return reverse('sub_agreements:dashboard')
        else:
            return base_home

    def clean_username(self, username):
        """ Renvoyer un nom d'utilisateur valide depuis un nom d'utilisateur passé """
        if not re.match(r'^[a-zA-Z0-9]+$', username):
            raise forms.ValidationError("Le nom d'utilisateur ne peut contenir que des lettres non-accentuées et des chiffres")
        if len(username) < 3:  # Minimum 3 caractères (correspond aux limites de l'API Openrunner)
            raise forms.ValidationError("Le nom d'utilisateur doit contenir au moins 3 (trois) caractères")
        return super(ManifsportAccountAdapter, self).clean_username(username)
