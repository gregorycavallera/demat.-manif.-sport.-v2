# coding: utf-8
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import UpdateView
from allauth.account.views import SignupView
from django.contrib.auth import get_user_model

from core.forms import UserUpdateForm, SignupAgentForm, SignupOrganisateurForm


class ProfileDetailView(DetailView):
    """ Vue du profil utilisateur """

    # Configuration
    model = get_user_model()

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class UserUpdateView(UpdateView):
    """ Modification du profil utilisateur """

    # Configuration
    model = get_user_model()
    form_class = UserUpdateForm
    success_url = '/accounts/profile/'

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class SignupAgentView(SignupView):
    form_class = SignupAgentForm
    template_name = "account/signupagent.html"
    view_name = 'signupagentview'

    def get_context_data(self, **kwargs):
        ret = super(SignupAgentView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret


signupagentview = SignupAgentView.as_view()


class SignupOrganisateurView(SignupView):
    form_class = SignupOrganisateurForm
    view_name = 'signuporganisateurview'

    def get_context_data(self, **kwargs):
        ret = super(SignupOrganisateurView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret


signuporganisateurview = SignupOrganisateurView.as_view()

class EmailConfirmedView(TemplateView):
    template_name = "account/email_confirmed.html"

    def get_context_data(self, **kwargs):
        context = super(EmailConfirmedView, self).get_context_data(**kwargs)
        user = self.request.user
        context['user'] = user
        return context
