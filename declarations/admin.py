# coding: utf-8
from django.contrib import admin

from .models import ManifestationDeclaration


class EventDeclarationInline(admin.StackedInline):
    """ Inline des déclarations """

    # Configuration
    model = ManifestationDeclaration
    extra = 0
    max_num = 1
    readonly_fields = ['creation_date']
