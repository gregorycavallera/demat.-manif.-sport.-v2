# coding: utf-8
import factory

from events.factories import DeclarationNMFactory

from .models import ManifestationDeclaration


class EventDeclarationFactory(factory.django.DjangoModelFactory):
    """ Factory des déclarations """

    # Champs
    manifestation = factory.SubFactory(DeclarationNMFactory)

    # Meta
    class Meta:
        model = ManifestationDeclaration
