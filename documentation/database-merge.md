Procédure préférée pour la fusion des bases de la version 2.18
=

Procédure
-

### Sur un poste de développement
Le poste de développement va servir deux buts :
- Fusionner les bases et tester l'application;
- générer un dump du résultat de la fusion, qui pourra être utilisé en production.

Pour la fusion, on utilise des dumps correspondant à l'état le plus récent 
des bases en production (42, 78 et 972). Généralement, il s'agira de l'état des 
bases après que l'application est mise hors-ligne.
On préfèrera les placer dans un répertoire facilement accessible, avec des noms simples tels que
"42", "78" et "972".

On utilise le système multi-bases de Django pour configurer 4 bases :
1. La base par défaut ('default'), qui contiendra le résultat de la fusion des autres bases.
2. La première base ('import_42'), qui contiendra les données du département 42.
3. Une seconde base ('import_78'), qui contiendra les données du département 78.
4. Une troisième base ('import 972') qui contiendra les données du département 972.


Une fois les 3 bases de données (42, 78 et 972) réunies dans un dossier facilement accessible
depuis la ligne de commande, il est possible d'automatiser toute l'opération de fusion.

Dans l'invite de commandes, vérifiez d'abord que vous êtes dans le virtualenv du projet.
Vérifiez également qu'aucun programme accédant aux bases de données cibles n'est ouvert.
Puis tapez :

```bash
dj full_merge chemin_base_42 chemin_base_78 chemin_base_972 [--no-input] [--dev] [--no-notifications]
```
Les chemins des bases peuvent être relatifs au répertoire en cours.
Ex.
```bash
dj full_merge dumps/42.sql dumps/78.sql dumps/972.sql
```

L'option `--no-input` permet de lancer l'opération sans confirmation par l'utilisateur.

L'option `--dev` permet, sur un poste de développement, de valider tous les utilisateurs
et de leur assigner un mot de passe par défaut. Assurez-vous à tout prix que cette commande
n'est pas accessible à un utilisateur non autorisé.

L'option `--no-notifications` permet, sur un poste de développement, de ne pas procéder
à l'importation et la fusion des données de notifications (cette option permet de gagner
environ 2/3 du temps nécessaire à l'importation).

__Note__ : dans settings.DATABASES, l'utilisateur de chaque base doit avoir le droit
de créer et supprimer des bases de données, et doit également être autorisé à
utiliser les commandes shell "dropdb" et "createdb" sans fournir de mot de passe.

L'opération de fusion va tourner pendant approximativement **25/35 minutes** (dont 15/30
pour l'import des notifications)


Vous pouvez ensuite créer un dump de la base fusionnée via la commande :
`pg_dump -U<utilisateur> main > merged-db.sql`

Il est conseillé également, suite à l'import, de définir `PROTECT_CODE = True` dans settings.
Cela permet d'empêcher l'exécution de certaines commandes sensibles, notamment en production
(où ce paramètre devrait être à `True` en permanence).


Les noms `default`, `import_42`, `import_78` et `import_972` sont les noms
des connexions définies dans `settings.DATABASES`. Ce ne sont pas nécessairement
les mêmes noms que les bases de données correspondantes dans PostgreSQL
(renseignées dans l'attribut `name`), qui elles,
sont au libre choix de l'utilisateur.
Ces informations devraient être présentes par défaut dans `configuration/settings/base.py`

La dernière étape de la fusion est manuelle et consiste à récupérer les dossiers des fichiers joints
de chacune des instances importées.

Les fichiers sont originellement disposés dans une arborescence de ce type :
`/media/<type de fichier>/<annee>/<mois>/<jour>/<nom de fichier>`

Pour chaque instance, il faut copier les fichiers situés dans /media/, et les
recopier dans le répertoire suivant :
`/media/<numéro de l'instance>/`

De façon à avoir une arborescence similaire à celle-ci :
`/media/<numéro de l'instance>/<type de fichier>/<annee>/<mois>/<jour>/<nom de fichier>`


### Historique des bugs
1. ~~Tous les bugs connus ont été résolus~~


### Addendum

Exporter le fichier de fixtures de divisions administratives :

```bash
./manage.py dumpdata administrative_division.commune administrative_division.arrondissement administrative_division.departement > administrative_division/fixtures/base_administrative_division.json
```
