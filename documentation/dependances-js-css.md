Inventaire des dépendances Javascript et CSS
===

!! Travail en cours !!

NB : les numéros de version mentionnés dans ce document peuvent ne pas avoir été actualisé lors des mises à jour.


### Bootstrap 4 Ti-Ta-Toggle

Permet d'embellir les cases à cocher.

Source : http://kleinejan.github.io/titatoggle/#intro

**Remarques**

- Compatible avec Bootstrap 4
- Ajout d'un fichier CSS seulement

**Installation**

- Copie de la librairie dans *portail/static/libs/titatoggle*
- Ajout de la dépendance dans le < head > du template *portail/templates/base.html*

Pour fonctionner, il est nécessaire que la balise < input > soit à l'intérieur de la balise < label > (ce qui est le cas 
avec django-crispy-forms) et qu'une balise <span> soit placée immédiatement après la balise < input >. Il est donc 
nécessaire de modifier le template d'affichage des balises < input > de django-crispy-forms.

- Copier le fichier env/lib/python3.6/site-packages/crispy_forms/templates/bootstrap4/field.html
et le coller dans templates/bootstrap4/field.html
- Ajouter la balise < span >
- Ajouter la classe "checkbox-slider--a" 


