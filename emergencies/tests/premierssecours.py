# coding: utf-8
from django.test import TestCase

from emergencies.models.premierssecours import Association1ersSecours
from emergencies.factories import Association1ersSecoursFactory


class AssociationPremiersSecoursTests(TestCase):
    """ Tests des associations de premiers secours """

    # Configuration
    def setUp(self):
        """ Préparer chaque test """
        self.association = Association1ersSecoursFactory.create()

    # Tests
    def test_str_(self):
        """ Tester le nom de l'objet """
        association = Association1ersSecoursFactory.build(name='Ordre de Malte')
        self.assertEqual(str(association), 'Ordre de Malte')

    def test_instance_empty(self):
        """ Tester les attributs département et instance """
        association = Association1ersSecours(name="Secours AAA", email="secoursaaa@aaa.aaa")
        self.assertIsNotNone(association.get_instance())
        self.assertTrue(association.get_instance().is_master())
        self.assertIsNone(association.get_departement())

    def test_instance_nonempty(self):
        """ Tester les attributs département et instance """
        self.assertIsNotNone(self.association.get_instance())
        self.assertFalse(self.association.get_instance().is_master())
        self.assertIsNotNone(self.association.get_departement())
