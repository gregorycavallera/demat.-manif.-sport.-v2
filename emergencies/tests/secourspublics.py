# coding: utf-8
from django.test import TestCase

from emergencies.factories import SecoursPublicsFactory
from emergencies.models.secourspublics import SecoursPublics


class SecoursPublicsTests(TestCase):
    """ Tests des associations de premiers secours """

    # Configuration
    def setUp(self):
        """ Préparer chaque test """
        self.secours = SecoursPublicsFactory.create()

    # Tests
    def test_str_(self):
        """ Tester le nom de l'objet """
        secours = SecoursPublicsFactory.build(name='Ordre de Malte')
        self.assertEqual(str(secours), 'Ordre de Malte')

    def test_instance_empty(self):
        """ Tester les attributs département et instance """
        secours = SecoursPublics(name="Secours AAA")
        self.assertIsNotNone(secours.get_instance())
        self.assertTrue(secours.get_instance().is_master())
        self.assertIsNone(secours.get_departement())

    def test_instance_nonempty(self):
        """ Tester les attributs département et instance """
        self.assertIsNotNone(self.secours.get_instance())
        self.assertFalse(self.secours.get_instance().is_master())
        self.assertIsNotNone(self.secours.get_departement())
