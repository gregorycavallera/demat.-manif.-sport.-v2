# coding: utf-8
from django.contrib import admin

from ..models import RNREvaluation


class RNREvaluationInline(admin.StackedInline):
    """ Inline d'évaluation RNR """

    # Configuration
    model = RNREvaluation
    extra = 0
