# coding: utf-8

import factory

from events.factories import DeclarationNMFactory
from evaluations.models import N2KEvaluation


class Natura2000EvaluationFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    range_from_site = 5
    track_total_length = 1
    manifestation = factory.SubFactory(DeclarationNMFactory)

    # Meta
    class Meta:
        model = N2KEvaluation
