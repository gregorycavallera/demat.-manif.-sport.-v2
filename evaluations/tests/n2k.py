# coding: utf-8
from django.test import TestCase

from evaluations.factories import Natura2000EvaluationFactory


class Natura2000EvaluationMethodTests(TestCase):
    def test_str_(self):
        '''
        __str__() should return the name of the evaluation
        '''
        n2000_evaluation = Natura2000EvaluationFactory.build()
        self.assertEqual(n2000_evaluation.__str__(), n2000_evaluation.manifestation.__str__())
