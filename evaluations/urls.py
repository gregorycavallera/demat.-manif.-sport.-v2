# coding: utf-8

from django.urls import path, re_path

from evaluations.views import Natura2000EvaluationCreate
from evaluations.views import Natura2000EvaluationUpdate
from evaluations.views import RNREvaluationCreate
from evaluations.views import RNREvaluationUpdate


app_name = 'evaluations'
urlpatterns = [
    # Évaluations
    re_path('natura2000evaluation/add/(?P<manifestation_pk>\d+)/', Natura2000EvaluationCreate.as_view(), name='natura2000evaluation_add'),
    re_path('natura2000evaluation/(?P<pk>\d+)/edit', Natura2000EvaluationUpdate.as_view(), name='natura2000evaluation_update'),
    re_path('rnrevaluation/add/(?P<manifestation_pk>\d+)/', RNREvaluationCreate.as_view(), name='rnrevaluation_add'),
    re_path('rnrevaluation/(?P<pk>\d+)/edit', RNREvaluationUpdate.as_view(), name='rnrevaluation_update'),
]
