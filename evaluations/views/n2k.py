# coding: utf-8
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from core.util.permissions import require_role
from events.models import Manifestation
from evaluations.forms import Natura2000EvaluationForm
from evaluations.models import N2KEvaluation


class Natura2000EvaluationCreate(CreateView):
    """ Création d'évaluation N2K """

    # Configuration
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if N2KEvaluation.objects.filter(manifestation=manifestation).exists():
            return redirect(manifestation.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        self.object = form.save(commit=False)
        self.object.manifestation = manifestation
        return super().form_valid(form)


class Natura2000EvaluationUpdate(UpdateView):
    """ Modification de l'évaluation N2K """

    # Configuration
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ N'autoriser l'accès qu'aux organisateurs """
        return super().dispatch(*args, **kwargs)
