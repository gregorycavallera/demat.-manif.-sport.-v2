# coding: utf-8
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from core.util.permissions import require_role
from evaluations.forms import RNREvaluationForm
from evaluations.models import RNREvaluation
from events.models import Manifestation


class RNREvaluationCreate(CreateView):
    """ Création d'évaluation RNR """

    # Configuration
    model = RNREvaluation
    form_class = RNREvaluationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if RNREvaluation.objects.filter(manifestation=manifestation).exists():
            return redirect(manifestation.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        self.object = form.save(commit=False)
        self.object.manifestation = manifestation
        return super().form_valid(form)


class RNREvaluationUpdate(UpdateView):
    """ Modification d'évaluation RNR """

    # Configuration
    model = RNREvaluation
    form_class = RNREvaluationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ N'autoriser l'accès qu'aux organisateurs """
        return super().dispatch(*args, **kwargs)
