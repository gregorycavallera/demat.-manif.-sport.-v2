# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from django.utils import timezone

class AskedListFilterMixin(admin.SimpleListFilter):
    """ Mixin pour les filtres """

    # Overrides
    def queryset(self, request, queryset):
        pass

    def lookups(self, request, model_admin):
        return ('yes', "oui"), ('no', "non")


class DeclarationAskedListFilter(AskedListFilterMixin):
    """ Filtre sur les demandes de déclarations """

    # Configuration
    title = "déclaration demandée"
    parameter_name = 'declaration_asked'

    # Override
    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(manifestationdeclaration__id__gt=0)
        elif self.value() == 'no':
            return queryset.exclude(manifestationdeclaration__id__gt=0)


class AuthorizationAskedListFilter(AskedListFilterMixin):
    """ Filtre sur les demandes d'autorisations """

    # Configuration
    title = "autorisations demandée"
    parameter_name = 'authorization_asked'

    # Overrides
    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(manifestationautorisation__id__gt=0)
        if self.value() == 'no':
            return queryset.exclude(manifestationautorisation__id__gt=0)


class ConcentrationAuthorizationAskedListFilter(AskedListFilterMixin):
    """ Filtre sur les demandes d'autorisation pour concentration de véhicules """

    # Configuration
    title = "autorisation ou déclaration demandée"
    parameter_name = 'authorization_or_declaration_asked'

    # Overrides
    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(Q(manifestationautorisation__id__gt=0) | Q(manifestationdeclaration__id__gt=0))
        if self.value() == 'no':
            return queryset.exclude(Q(manifestationautorisation__id__gt=0) | Q(manifestationdeclaration__id__gt=0))


class ProcessingFilter(admin.SimpleListFilter):
    title = 'Formulaire validé'
    parameter_name = 'processing'

    def lookups(self, request, model_admin):
        return ('True', 'Formulaire validé'), ('False', 'Formulaire non validé')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(Q(manifestationautorisation__id__gt=0) | Q(manifestationdeclaration__id__gt=0))
        if self.value() == 'False':
            return queryset.exclude(Q(manifestationautorisation__id__gt=0) | Q(manifestationdeclaration__id__gt=0))


class CalendarFilter(admin.SimpleListFilter):
    title = 'Présence dans le calendrier'
    parameter_name = 'In calendar'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Non')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(Q(private=False) & Q(hidden=False))
        if self.value() == 'False':
            return queryset.filter(Q(private=True) | Q(hidden=True))


class ComingFilter(admin.SimpleListFilter):
    title = 'A venir'
    parameter_name = 'coming'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Passée')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(begin_date__gte=timezone.now())
        if self.value() == 'False':
            return queryset.filter(begin_date__lt=timezone.now())
