# coding: utf-8
from ajax_select.fields import AutoCompleteField
from crispy_forms.layout import Layout, Fieldset, HTML
from django.forms import ModelChoiceField
from django.template.loader import render_to_string
from django import forms

from core.forms.base import GenericForm
from events.forms.manifestation import FORM_WIDGETS, HELP_EXTRA_ACTIVITE
from events.models import DeclarationNM
from sports.models import Discipline
from .manifestation import ManifestationForm, FIELDS_MANIFESTATION, FIELDS_ROADS, FIELDS_FILES, FIELDS_MARKUP, FIELDS_MARKUP_CONV, FIELDS_NATURA2000


class DeclarationNMForm(ManifestationForm):
    """ Formulaire """
    FORM_WIDGETS['city_police_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px'})
    FORM_WIDGETS['state_police_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px'})

    # Champs
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=False))
    extra_activite = AutoCompleteField('activite_nm', label="Recherche d'activité sportive", required=False,
                                       help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DeclarationNMForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Remplissez ce formulaire puis vous pourrez joindre ensuite des fichiers à votre dossier.</p><hr>"),
                                    Fieldset(*(FIELDS_MANIFESTATION + ['grouped_traffic', 'grouped_start', 'grouped_run', 'support_vehicles_number'])),
                                    Fieldset("Coordonnateur Sécurité", 'safety_name', 'safety_firstname', 'safety_tel', 'safety_email'),
                                    Fieldset(*FIELDS_ROADS),
                                    Fieldset("Informations sur le dispositif de sécurité de la manifestation",
                                             Fieldset("Véhicules d'accompagnement :", 'opening_vehicle', 'heading_vehicle', 'trailing_vehicle', 'staff_vehicle', css_class='special_fieldset'),
                                             Fieldset("Signaleurs :", 'signalers_number', 'signalers_number_still', 'signalers_number_auto', 'signalers_number_moto', css_class='special_fieldset'),
                                             Fieldset("Forces de l'ordre :", 'city_police', 'city_police_detail', 'state_police', 'state_police_detail', css_class='special_fieldset')
                                             ),
                                    Fieldset(*FIELDS_MARKUP_CONV), Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(FIELDS_NATURA2000[:-4])))

    # Meta
    class Meta:
        model = DeclarationNM
        exclude = ('structure', 'big_title', 'approval_request', 'motor_on_closed_road', 'motor_on_natura2000', 'rounds_safety', 'instance')
        help_texts = {'organisateur_commitment': render_to_string('events/forms/help/dnm_organisateur_commitment.txt')}
        widgets = FORM_WIDGETS


class DeclarationNMFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES[:-1])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = DeclarationNM
        fields = ['manifestation_rules', 'organisateur_commitment', 'insurance_certificate', 'safety_provisions', 'doctor_attendance', 'additional_docs',
                  'rounds_safety']
