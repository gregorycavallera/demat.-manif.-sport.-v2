# coding: utf-8
import ast
import logging
from datetime import timedelta

from ajax_select.fields import AutoCompleteField
from bootstrap_datepicker_plus import DateTimePickerInput
from django import forms
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from django.forms import ModelChoiceField
from django.forms.models import ModelMultipleChoiceField
from django.forms.widgets import SelectMultiple
from django.utils import timezone
from crispy_forms.layout import HTML

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField, ChainedModelMultipleChoiceField
from clever_selects.forms import ChainedChoicesModelForm
from core.forms.base import GenericForm
from events.models.manifestation import Manifestation
from sports.models import Discipline
from sports.models.sport import Activite


logger = logging.getLogger('django.request')

# Contenus de Fieldsets standards
# Modifiés à la volée dans les helpers des formulaires hérités
FIELDS_MANIFESTATION = ["Détail de la manifestation", 'name', 'begin_date', 'end_date', 'description', 'observation', 'private', 'show_structure_address', 'extra_activite', 'discipline',
                        'activite', 'registered_calendar', 'is_on_rnr', 'departure_departement', 'departure_city', 'other_departments_crossed',
                        'crossed_cities', 'openrunner_route', 'route_descriptions', 'number_of_entries', 'max_audience']
FIELDS_ROADS = ["Gestion des voies publiques", 'oneway_roads', 'closed_roads']
FIELDS_FILES = ["Fichiers attachés", 'manifestation_rules', 'organisateur_commitment', 'insurance_certificate', 'safety_provisions', 'doctor_attendance',
                'additional_docs', 'rounds_safety']
FIELDS_MARKUP_CONV = ["Charte du balisage temporaire", 'markup_convention']
FIELDS_MARKUP = ["Type de balisage utilisé", 'chalk_markup', 'rubalise_markup', 'bio_rubalise_markup', 'paneling_markup', 'ground_markup', 'sticker_markup',
                 'lime_markup', 'plaster_markup', 'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup']
FIELDS_NATURA2000 = ["Évaluation d'incidences Natura2000", HTML('Cochez une de ces cases si concerné, vous devrez alors saisir un formulaire N2000 avant de demander l\'autorisation.'), 'big_budget', 'lucrative', 'big_title', 'motor_on_natura2000', 'motor_on_closed_road',
                     'approval_request']

# Dates par défaut de début et fin de manifestation
DEFAULT_START = timezone.now() + timedelta(days=150)
# Inversion jour et mois pour datepicker4
DEFAULT_START_STR = DEFAULT_START.strftime('%m/%d/%Y 8:00')
DEFAULT_END_STR = DEFAULT_START.strftime('%m/%d/%Y 20:00')

# Widgets
FORM_WIDGETS = {
    'begin_date': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_START_STR, 'locale': 'fr'}),
    'end_date': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_END_STR, 'locale': 'fr'}),
    'openrunner_route': SelectMultiple(attrs={"size": 10}),
    'description': forms.Textarea(attrs={'rows': 1}),
    'observation': forms.Textarea(attrs={'rows': 1}),
    'route_descriptions': forms.Textarea(attrs={'rows': 1}),
    'oneway_roads': forms.Textarea(attrs={'rows': 1}),
    'closed_roads': forms.Textarea(attrs={'rows': 1}),
}

# Textes d'aide
HELP_EXTRA_ACTIVITE = "recherchez votre activité sportive dans ce champ. Cliquez ensuite sur l'activité pour remplir automatiquement les champs ci-dessous."


class ManifestationForm(GenericForm, ChainedChoicesModelForm):
    """ Création de manifestation """

    # Champs
    extra_activite = AutoCompleteField('activite', label="Recherche d'activité sportive", required=False,
                                       help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)
    discipline = ModelChoiceField(required=False, queryset=Discipline.objects.all())
    activite = ChainedModelChoiceField('discipline', reverse_lazy('sports:activite_widget'), Activite)
    departure_departement = ModelChoiceField(required=False, queryset=Departement.objects.configured(), label="Département de départ")
    departure_city = ChainedModelChoiceField('departure_departement', reverse_lazy('administrative_division:commune_widget'), Commune,
                                             label="Commune de départ")
    other_departments_crossed = ModelMultipleChoiceField(required=False, queryset=Departement.objects.all(), label="Autres départements traversés", help_text="attention : si votre manifestation se déroule sur plusieurs départements, alors le délai d'instruction est de 3 mois !")
    crossed_cities = ChainedModelMultipleChoiceField('other_departments_crossed',
                                                     reverse_lazy('administrative_division:commune_widget'),
                                                     Commune, label="Autres communes traversées", required=False,
                                                     help_text="Ne pas sélectionner la commune de départ.")

    # Overrides
    def __init__(self, *args, **kwargs):
        self.routes = kwargs.pop('routes', None)
        self.extradata = kwargs.pop('extradata', None)
        super().__init__(*args, **kwargs)
        if self.routes and 'openrunner_route' in self.fields:
            self.fields['openrunner_route'].widget.choices = list(self.routes.items())
            self.fields['openrunner_route'].widget.attrs = {'data-placeholder': "Sélectionnez les parcours pour cette manifestation"}
        if hasattr(self, 'helper'):
            self.helper.form_tag = False
        self.fields['other_departments_crossed'].widget.attrs = {'data-placeholder': "1. Sélectionnez les départements traversés"}
        self.fields['crossed_cities'].widget.attrs = {'data-placeholder': "2. Sélectionnez les autres communes traversées"}
        self.fields['activite'].widget.attrs = {'data-placeholder': "Choisissez une discipline"}

    # Validation
    def clean_openrunner_route(self):
        """ Nettoyer et valider le contenu de openrunner_route """
        # Les données sont une chaîne au format "[a,b,c,d...]"
        openrunner_route = self.cleaned_data['openrunner_route']
        try:
            # Convertir cette chaîne en type primitif python (liste d'entiers)
            # Renvoyer les routes dans un format compatible avec CommaSeparatedField
            openrunner_route = ast.literal_eval(openrunner_route)
            return ','.join(openrunner_route)
        except (SyntaxError, TypeError):
            return ''

    def clean_begin_date(self):
        """ Valider la date de départ """
        begin_date = self.cleaned_data['begin_date']
        if not hasattr(self, 'instance') or not self.instance or not self.instance.id:
            if begin_date < timezone.now():
                raise ValidationError("Le date de départ de la manifestation ne peut pas être dans le passé")
        return begin_date

    def clean(self):
        cleaned_data = super().clean()
        begin_date = cleaned_data.get('begin_date')
        end_date = cleaned_data.get('end_date')
        if begin_date and end_date and begin_date >= end_date:
            message = "La date de fin de la manifestation doit être supérieure à la date de début."
            self.add_error("end_date", self.error_class([message]))
        if begin_date and end_date and end_date >= begin_date + timedelta(days=90):
            message = "La manifestation ne peut excéder 90 jours."
            self.add_error("end_date", self.error_class([message]))

        # Vérifier que "departure_city" n'est pas dans la liste de "crossed_cities"
        depart = cleaned_data.get('departure_city')
        liste = list(cleaned_data.get('crossed_cities'))
        if liste:
            cleaned_data['crossed_cities'] = [x for x in liste if x != depart]
        return cleaned_data

    class Meta:
        model = Manifestation
        exclude = []


class ManifestationInstructeurUpdateForm(GenericForm):
    """ Formulaire destiné aux instructeurs """

    class Meta:
        model = Manifestation
        fields = ['show_structure_address', 'hidden']

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self, 'helper'):
            self.helper.form_tag = False


class ManifestationFilesForm(GenericForm):
    """ Formulaire d'upload des fichiers d'une manifestation """

    class Meta:
        model = Manifestation
        widgets = FORM_WIDGETS
        fields = ['manifestation_rules', 'organisateur_commitment', 'insurance_certificate', 'safety_provisions', 'doctor_attendance', 'additional_docs',
                  'rounds_safety']
