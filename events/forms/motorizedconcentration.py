# coding: utf-8
from ajax_select.fields import AutoCompleteField
from crispy_forms.layout import Layout, Fieldset, HTML
from django.forms import ModelChoiceField
from django.template.loader import render_to_string

from core.forms.base import GenericForm
from events.forms.manifestation import FORM_WIDGETS, HELP_EXTRA_ACTIVITE
from events.models import MotorizedConcentration
from sports.models import Discipline
from .manifestation import ManifestationForm, FIELDS_MANIFESTATION, FIELDS_ROADS, FIELDS_FILES, FIELDS_MARKUP, FIELDS_MARKUP_CONV, FIELDS_NATURA2000


class MotorizedConcentrationForm(ManifestationForm):
    """ Formulaire """

    # Champs
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=True))
    extra_activite = AutoCompleteField('activite_m', label="Recherche d'activité sportive", required=False,
                                       help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super(MotorizedConcentrationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Remplissez ce formulaire puis vous pourrez joindre ensuite des fichiers à votre dossier.</p><hr>"),
                                    Fieldset("Organisateur Technique", 'tech_name', 'tech_firstname', 'tech_tel', 'tech_email'),
                                    Fieldset(*(FIELDS_MANIFESTATION[:-1] + ['meeting', 'max_audience', 'vehicles', 'big_concentration', 'following'])),
                                    Fieldset(*FIELDS_ROADS),
                                    Fieldset(*FIELDS_MARKUP_CONV), Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(FIELDS_NATURA2000[:-2])))

    # Meta
    class Meta:
        model = MotorizedConcentration
        exclude = ('structure', 'approval_request', 'motor_on_closed_road', 'instance')
        help_texts = {'organisateur_commitment': render_to_string('events/forms/help/mc_organisateur_commitment.txt')}
        widgets = FORM_WIDGETS


class MotorizedConcentrationFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES + ['cartography'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = MotorizedConcentration
        fields = ['manifestation_rules', 'organisateur_commitment', 'insurance_certificate', 'safety_provisions',
                  'doctor_attendance', 'additional_docs', 'rounds_safety', 'cartography']
