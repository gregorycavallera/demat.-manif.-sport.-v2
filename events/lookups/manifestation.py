# coding: utf-8
from ajax_select import register, LookupChannel
from unidecode import unidecode

from events.models.manifestation import Manifestation
from organisateurs.models.structure import Structure


@register('manifestation')
class ManifestationLookup(LookupChannel):
    """ Lookup AJAX des manifestations """

    # Configuration
    model = Manifestation

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        return self.model.objects.filter(instance__isnull=False, name__icontains=q).order_by('name')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>{name}</span>".format(name=item.name)

    def format_match(self, item):
        return u"<span class='tag'>{name}</span>".format(name=item.name)
