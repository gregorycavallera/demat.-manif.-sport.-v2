# coding: utf-8
from django.conf import settings
from django.core.management.base import BaseCommand

from core.util.security import protect_code
from events.models.manifestation import Manifestation


class Command(BaseCommand):
    """ Corrige des données incorrectes de manifestations """
    args = ''
    help = 'Modifier les données de manifestation incorrectes'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        if settings.DEBUG:
            manifestations = Manifestation.objects.filter(number_of_entries__lt=0)
            count = manifestations.count()
            for manifestation in manifestations:
                manifestation.number_of_entries = abs(manifestation.number_of_entries)
                manifestation.save(force_update=True, update_fields=['number_of_entries'])
            print("{0} manifestations ont été corrigées".format(count))
        else:
            print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
