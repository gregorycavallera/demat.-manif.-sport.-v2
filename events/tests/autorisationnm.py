# coding: utf-8
import datetime

from django.utils.timezone import utc

from administrative_division.factories import DepartementFactory
from authorizations.factories import ManifestationAuthorizationFactory
from events.tests.base import EventsTestsBase
from notifications.models import Action
from ..factories import AutorisationNMFactory


class AutorisationNMTests(EventsTestsBase):
    """ Tests"""

    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = AutorisationNMFactory.build(pk=3)
        self.assertEqual(manifestation.get_absolute_url(), '/AutorisationNM/3/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        manifestation_a = AutorisationNMFactory.build(lucrative=True)
        self.assertTrue(manifestation_a.display_natura2000_eval_panel())
        manifestation_b = AutorisationNMFactory.build(big_budget=True)
        self.assertTrue(manifestation_b.display_natura2000_eval_panel())
        manifestation_c = AutorisationNMFactory.build(big_title=True)
        self.assertTrue(manifestation_c.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = AutorisationNMFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_manifestation_log_creation(self):
        manifestation = AutorisationNMFactory.create()
        action_count = Action.objects.count()
        action = Action.objects.last()
        self.assertEqual(action.user, manifestation.structure.organisateur.user)
        self.assertEqual(action.manifestation, manifestation.manifestation_ptr)
        self.assertEqual(action.action, "description de la manifestation")
        # Vérifier qu'aucune nouvelle action n'a été créée
        manifestation.save()
        self.assertEqual(action_count, Action.objects.count())

    def test_legal_delay(self):
        manifestation = AutorisationNMFactory.create()
        self.assertEqual(manifestation.legal_delay(), 60)

    def test_legal_delay_2(self):
        manifestation = AutorisationNMFactory.create()
        DepartementFactory.create()
        manifestation.other_departments_crossed.add(DepartementFactory.create())
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_get_limit_date(self):
        manifestation = AutorisationNMFactory.create(begin_date=datetime.datetime.utcnow().replace(tzinfo=utc))
        self.assertEqual(
            manifestation.get_limit_date(),
            (manifestation.begin_date - datetime.timedelta(days=60)).replace(hour=23, minute=59)
        )

    def test_delay_exceeded(self):
        manifestation = AutorisationNMFactory.create(begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=59)))
        self.assertTrue(manifestation.delay_exceeded())

    def test_delay_not_exceeded(self):
        manifestation = AutorisationNMFactory.create(begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=61)))
        self.assertFalse(manifestation.delay_exceeded())

    def test_not_two_weeks_left(self):
        manifestation = AutorisationNMFactory.create(begin_date=(datetime.datetime.today() + datetime.timedelta(days=60) + datetime.timedelta(weeks=2)))
        self.assertFalse(manifestation.two_weeks_left())

    def test_two_weeks_left(self):
        manifestation = AutorisationNMFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=60) + datetime.timedelta(weeks=1))
        )
        self.assertTrue(manifestation.two_weeks_left())

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = AutorisationNMFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = AutorisationNMFactory.create()
        ManifestationAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)
