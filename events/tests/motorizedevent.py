# coding: utf-8

from authorizations.factories import ManifestationAuthorizationFactory
from core.factories.instance import InstanceFactory
from events.tests.base import EventsTestsBase
from ..factories import MotorizedEventFactory


class MotorizedEventMethodTests(EventsTestsBase):
    """ Tests"""

    # Tests
    def test_get_absolute_url(self):
        manifestation = MotorizedEventFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/motorizedevents/4/')

    def test_display_natura2000_eval_panel(self):
        manifestation = MotorizedEventFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = MotorizedEventFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = MotorizedEventFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())
        manifestation5 = MotorizedEventFactory.build(motor_on_natura2000=True)
        self.assertTrue(manifestation5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = MotorizedEventFactory.build(instance=InstanceFactory.build())
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay(self):
        manifestation = MotorizedEventFactory.build(instance=InstanceFactory.build())
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = MotorizedEventFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = MotorizedEventFactory.create()
        ManifestationAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)
