# coding: utf-8
from django.test import TestCase

from events.factories import UnsupportedManifestationFactory
from events.models.unsupported import UnsupportedManifestation


class UnsupportedManifestationTests(TestCase):

    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.manifestation = UnsupportedManifestationFactory.create()

    def test_queryset(self):
        """ Tester qu'il existe bien une manifestation non prise en charge dans la base  """
        self.assertTrue(UnsupportedManifestation.objects.all().exists())

    def test_supported(self):
        """ Tester la prise en charge """
        self.assertTrue(self.manifestation.is_unsupported())
