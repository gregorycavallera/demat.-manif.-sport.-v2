# coding: utf-8
from events.views.manifestation import ManifestationDetail, ManifestationCreate, ManifestationUpdate, ManifestationFilesUpdate
from ..forms import *
from ..models import *


class AutorisationNMDetail(ManifestationDetail):
    """ Détail de manifestation """

    # Configuration
    model = AutorisationNM


class AutorisationNMCreate(ManifestationCreate):
    """ Création d'une manifestation """

    # Configuration
    model = AutorisationNM
    form_class = AutorisationNMForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'anm'
        return context


class AutorisationNMUpdate(ManifestationUpdate):
    """ Modification d'une manifestation """

    # Configuration
    model = AutorisationNM
    form_class = AutorisationNMForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'anm'
        return context


class AutorisationNMFilesUpdate(ManifestationFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = AutorisationNM
    form_class = AutorisationNMFilesForm
