# coding: utf-8

from events.views.manifestation import ManifestationDetail, ManifestationCreate, ManifestationUpdate, ManifestationFilesUpdate
from ..forms import *
from ..models import *


class MotorizedConcentrationDetail(ManifestationDetail):
    """ Détail de la manifestation """

    # Configuration
    model = MotorizedConcentration


class MotorizedConcentrationCreate(ManifestationCreate):
    """ Création de la manifestation """

    # Configuration
    model = MotorizedConcentration
    form_class = MotorizedConcentrationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtmc'
        return context


class MotorizedConcentrationUpdate(ManifestationUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = MotorizedConcentration
    form_class = MotorizedConcentrationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtmc'
        return context


class MotorizedConcentrationFilesUpdate(ManifestationFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = MotorizedConcentration
    form_class = MotorizedConcentrationFilesForm
