# coding: utf-8

from events.views.manifestation import ManifestationDetail, ManifestationCreate, ManifestationUpdate, ManifestationFilesUpdate
from ..forms import *
from ..models import *


class MotorizedEventDetail(ManifestationDetail):
    """ Détail de manifestation """

    # Confugration
    model = MotorizedEvent


class MotorizedEventCreate(ManifestationCreate):
    """ Création de manifestation """

    # Configuration
    model = MotorizedEvent
    form_class = MotorizedEventForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtm'
        return context


class MotorizedEventUpdate(ManifestationUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = MotorizedEvent
    form_class = MotorizedEventForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtm'
        return context


class MotorizedEventFilesUpdate(ManifestationFilesUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = MotorizedEvent
    form_class = MotorizedEventFilesForm
