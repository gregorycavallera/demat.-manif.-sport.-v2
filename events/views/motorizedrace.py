# coding: utf-8

from events.views.manifestation import ManifestationDetail, ManifestationCreate, ManifestationUpdate, ManifestationFilesUpdate
from ..forms import *
from ..models import *


class MotorizedRaceDetail(ManifestationDetail):
    """ Détail de la manifestation """

    # Configiratiuon
    model = MotorizedRace


class MotorizedRaceCreate(ManifestationCreate):
    """ Création de la manifestation """

    # Configuration
    model = MotorizedRace
    form_class = MotorizedRaceForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtmcir'
        return context


class MotorizedRaceUpdate(ManifestationUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = MotorizedRace
    form_class = MotorizedRaceForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtmcir'
        return context


class MotorizedRaceFilesUpdate(ManifestationFilesUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = MotorizedRace
    form_class = MotorizedRaceFilesForm
