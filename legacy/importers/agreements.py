# coding: utf-8
from functools import lru_cache

from agreements.models.avis import Avis
from agreements.models.cg import CGAvis
from agreements.models.ddsp import DDSPAvis
from agreements.models.edsr import EDSRAvis
from agreements.models.federation import FederationAvis
from agreements.models.ggd import GGDAvis
from agreements.models.mairie import MairieAvis
from agreements.models.sdis import SDISAvis
from agreements.models.service import ServiceAvis
from legacy.importers.autorisations import AutorisationImporter
from legacy.importers.base import BaseImporter
from legacy.importers.services import ServiceImporter


class AvisImporter(BaseImporter):
    """ Importer les avis """

    # Attributs
    name = "Avis"
    description = "Importation des avis"
    models = ['agreements.avis']

    # Getter
    @staticmethod
    @lru_cache(maxsize=2048)
    def get_avis(name):
        return Avis.objects.get(authorization__manifestation__name=name)

    # Overrides
    def process(self, processor):
        """ Importer les avis """

        # Avis
        for item in Avis.objects.using(self.source).all():
            # Récupérer l'avis précis de service
            if item.attached_document.name:
                item.attached_document.name = "{number}/{base}".format(number=self.number, base=item.attached_document.name)
                item.save()

            # La particularité ici est qu'il est impossible d'identifier un Avis via ses champs (autres que id)
            cg_items = list(CGAvis.objects.using(self.source).filter(avis_ptr=item))
            ddsp_items = list(DDSPAvis.objects.using(self.source).filter(avis_ptr=item))
            edsr_items = list(EDSRAvis.objects.using(self.source).filter(avis_ptr=item))
            federation_items = list(FederationAvis.objects.using(self.source).filter(avis_ptr=item))
            ggd_items = list(GGDAvis.objects.using(self.source).filter(avis_ptr=item))
            mairie_items = list(MairieAvis.objects.using(self.source).filter(avis_ptr=item))
            sdis_items = list(SDISAvis.objects.using(self.source).filter(avis_ptr=item))
            service_items = list(ServiceAvis.objects.using(self.source).filter(avis_ptr=item))
            # Sauvegarder l'avis de base
            item.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
            item.pk = None
            item.save(using=self.destination)
            # Lier les avis de service
            # CG
            if cg_items:
                for sub in cg_items:
                    services = list(sub.concerned_services.all().values_list('cg__departement__name', 'name'))
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)
                    sub.concerned_services.clear()
                    for service in services:
                        sub.concerned_services.add(ServiceImporter.get_cg_service(service[0], service[1]))
            # DDSP
            if ddsp_items:
                for sub in ddsp_items:
                    services = list(sub.commissariats_concernes.all().values_list('commune__code', 'commune__name'))
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)
                    sub.commissariats_concernes.clear()
                    for service in services:
                        sub.commissariats_concernes.add(ServiceImporter.get_commissariat(service[0], service[1]))
            # EDSR
            if edsr_items:
                for sub in edsr_items:
                    services = list(sub.concerned_cgd.all().values_list('arrondissement__code', flat=True))
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)
                    sub.concerned_cgd.clear()
                    for service in services:
                        sub.concerned_cgd.add(ServiceImporter.get_cgd(service))
            # Fédération
            if federation_items:
                for sub in federation_items:
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)
            # GGD
            if ggd_items:
                for sub in ggd_items:
                    cgd = list(sub.concerned_cgd.all().values_list('arrondissement__code', flat=True))
                    sub.concerned_edsr_id = ServiceImporter.get_edsr(sub.concerned_edsr.departement.name).pk
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)
                    sub.concerned_cgd.clear()
                    for cgd_item in cgd:
                        sub.concerned_cgd.add(ServiceImporter.get_cgd(cgd_item))
            # Mairie
            if mairie_items:
                for sub in mairie_items:
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.commune_id = ServiceImporter.get_commune(sub.commune.code, sub.commune.name).pk
                    sub.save(using=self.destination)
            # SDIS
            if sdis_items:
                for sub in sdis_items:
                    services = list(sub.compagnies_concernees.all().values_list('sdis__departement__name', 'number'))
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)
                    sub.compagnies_concernees.clear()
                    for service in services:
                        sub.compagnies_concernees.add(ServiceImporter.get_compagnie(service[0], service[1]))
            # Service
            if service_items:
                for sub in service_items:
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.service_id = ServiceImporter.get_service(sub.service.departement.name, sub.service.name).pk
                    sub.id = item.pk
                    sub.authorization_id = AutorisationImporter.get_autorisation(item.authorization.manifestation.name).pk
                    sub.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
