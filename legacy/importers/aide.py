# coding: utf-8
from django.contrib.auth.models import Group, Permission
from django.contrib.flatpages.models import FlatPage

from aide.models.help import HelpPage
from legacy.importers.base import BaseImporter
from legacy.importers.services import ServiceImporter


class AideImporter(BaseImporter):
    """ Importer les emails et confirmations """

    # Attributs
    name = "Aide"
    description = "Importation des flatpages vers le système d'aide"
    models = ['django.flatpage']

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # Flatpages
        for item in FlatPage.objects.using(self.source).all():

            if self.number in ['78', '972'] and '/78/' not in item.path and '/972/' not in item.path:
                item.url = "{0}/{1}/".format(item.url, self.number)  # Suffixer si besoin les urls pour 78 et 972

            if not HelpPage.objects.filter(path=item.url):
                aide = HelpPage()
                aide.title = item.title
                aide.path = item.url
                aide.content = item.content
                aide.authenticated_only = item.registration_required
                aide.save()
                if '/42/' in item.url or '/78/' in item.url or '/972/' in item.url:
                    aide.departements.add(ServiceImporter.get_departement(self.number))

    def post_process(self):
        """ Modification après la première passe """
        group_name = 'documentalistes'
        commenter_name = 'annotateurs'
        if not Group.objects.filter(name=group_name).exists():
            # Créer un groupe documentalistes avec les droits sur les helppages
            group = Group.objects.create(name=group_name)
            # Ajouter les droits Helppage
            can_change = Permission.objects.get(content_type__app_label='aide', content_type__model='helppage', codename='change_helppage')
            can_add = Permission.objects.get(content_type__app_label='aide', content_type__model='helppage', codename='add_helppage')
            can_delete = Permission.objects.get(content_type__app_label='aide', content_type__model='helppage', codename='delete_helppage')
            group.permissions.add(can_change, can_add, can_delete)

            # Créer un groupe annotateurs avec les droits sur les helpnotes
            group = Group.objects.create(name=commenter_name)
            # Ajouter les droits Helpnote
            can_change = Permission.objects.get(content_type__app_label='aide', content_type__model='helpnote', codename='change_helpnote')
            can_add = Permission.objects.get(content_type__app_label='aide', content_type__model='helpnote', codename='add_helpnote')
            can_delete = Permission.objects.get(content_type__app_label='aide', content_type__model='helpnote', codename='delete_helpnote')
            group.permissions.add(can_change, can_add, can_delete)
