# coding: utf-8
from allauth.account.models import EmailAddress, EmailConfirmation

from legacy.importers.base import BaseImporter
from legacy.importers.users import UserImporter


class AllauthImporter(BaseImporter):
    """ Importer les emails et confirmations """

    # Attributs
    name = "Allauth"
    description = "Importation des emails et validations"
    models = ['account.emailaddress', 'account.emailconfirmation']

    # Getter
    @staticmethod
    def get_email(name):
        return EmailAddress.objects.filter(email=name).first()

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # Adresses email
        for item in EmailAddress.objects.using(self.source).all():
            if not EmailAddress.objects.using(self.destination).filter(email=item.email).exists():
                item.user_id = UserImporter.get_user(item.user.username).pk
                item.pk = None
                item.save(using=self.destination)

        # Jeton de validation par email
        for item in EmailConfirmation.objects.using(self.source).all():
            item.email_address_id = self.get_email(item.email_address.email).pk
            item.pk = None
            item.save(using=self.destination, force_insert=True)

    def post_process(self):
        """ Modification après la première passe"""
        pass
