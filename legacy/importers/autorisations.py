# coding: utf-8
from functools import lru_cache

from authorizations.models.autorisation import ManifestationAutorisation
from legacy.importers.base import BaseImporter
from legacy.importers.manifestation import ManifestationImporter
from legacy.importers.services import ServiceImporter


class AutorisationImporter(BaseImporter):
    """ Importer les autorisations """

    # Attributs
    name = "Autorisations de Manifestations"
    description = "Importation des autorisations"
    models = ['authorizations.manifestationautorisation']

    # Getter
    @staticmethod
    @lru_cache(maxsize=4096)
    def get_autorisation(name):
        return ManifestationAutorisation.objects.get(manifestation__name=name)

    # Overrides
    def process(self, processor):
        """ Importer les autorisations """

        # Déclarations
        for item in ManifestationAutorisation.objects.using(self.source).all():
            if not ManifestationAutorisation.objects.filter(manifestation__name=item.manifestation.name).exists():
                services = list(item.concerned_services.all().values_list('departement__name', 'name'))
                communes = list(item.concerned_cities.all().values_list('code', 'name'))
                item.manifestation_id = ManifestationImporter.get_manifestation(item.manifestation.name).pk
                if item.bylaw.name:
                    item.bylaw.name = "{number}/{base}".format(number=self.number, base=item.bylaw.name)
                item.pk = None
                item.save(using=self.destination)
                item.concerned_cities.clear()
                item.concerned_services.clear()
                for service in services:
                    item.concerned_services.add(ServiceImporter.get_service(service[0], service[1]))
                for commune in communes:
                    item.concerned_cities.add(ServiceImporter.get_commune(commune[0], commune[1]))

    def post_process(self):
        """ Modification après la première passe"""
        pass
