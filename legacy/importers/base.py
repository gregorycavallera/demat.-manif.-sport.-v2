# coding: utf-8
from django.conf import settings
from django.db import transaction


class BaseImporter(object):
    """ Éléments de base pour l'import depuis une autre base """

    # Attributs
    name = None
    source = 'import'
    destination = 'default'
    description = "Importation"
    number = None  # str
    models = []  # liste de modèles importés, par nom

    # Initialiser
    def __init__(self, source=None, number=None):
        """ Initialiser l'importeur de document """
        super().__init__()
        self.source = source or self.source
        self.number = number or self.number
        BaseImporter.source = self.source

    # Getter
    def get_model_count(self):
        """ Revnoyer le nombre de modèles traités par cet importateur """
        return len(self.models)

    def get_related(self, instance):
        """ Renvoie toutes les instances d'objet liées à l'objet passé, uniquement si dans self.models """
        result = set()
        # Récupérer les accesseurs qui seront appelés pour récupérer les objets
        links = [rel.get_accessor_name() for rel in instance._meta.get_all_related_objects()]
        # Récupérer les accesseurs, tous les objets des accesseurs, et les ajouter
        for link in links:
            try:
                items = getattr(instance, link).all()
                for item in items:
                    if item._meta.label_lower in self.models:
                        result.add(item)
            except AttributeError:
                try:
                    item = getattr(instance, link)
                    if item._meta.label_lower in self.models:
                        result.add(item)
                except AttributeError:
                    pass
        return result

    # Méthodes
    def process(self, processor):
        """
        Importer les données

        :returns: un dictionnaire de données à conserver dans le BaseProcessor
        """
        raise NotImplementedError()

    def post_process(self):
        """ Importer des données supplémentaires """
        pass


class BaseProcessor(object):
    """ Scénario d'import """

    # Attributs
    data = dict()  # données
    statistics = dict()
    ordering = []  # liste de BaseImporterd
    sources = ['import']
    destination = 'default'

    # Process
    def process(self):
        for source in self.sources:
            print("=" * 60)
            print("IMPORT DE LA BASE DE DONNÉES {name}".format(name=source))
            print("=" * 60)

            number = settings.DATABASES[source]['NUMBER']
            with transaction.atomic():
                for i, importer_class in enumerate(self.ordering, start=1):
                    importer = importer_class(source, number)
                    print("{i}/{t} : Importation des données de {name}".format(name=importer_class.name, i=i, t=len(self.ordering)))
                    data = importer.process(self) or {}
                    try:
                        self.data.update(data)
                    except:
                        pass
        for i, importer_class in enumerate(self.ordering, start=1):
            print("{i}/{t} : Ajustements post-fusion de {name}".format(name=importer_class.name, i=i, t=len(self.ordering)))
            importer = importer_class('default', None)
            importer.post_process()
