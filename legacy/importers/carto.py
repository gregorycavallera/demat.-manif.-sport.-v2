# coding: utf-8

from legacy.importers.base import BaseImporter
from organisateurs.models.organisateur import Organisateur


class CartoImporter(BaseImporter):
    """ Importer les comptes OpenRunner """

    # Attributs
    name = "Cartographie"
    description = "Importation des comptes OpenRunner"
    models = ['carto.compteopenrunner']

    # Getter
    @staticmethod
    def get_organisateur(name):
        return Organisateur.objects.get(user__username=name)

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # Organisateurs
        pass

    def post_process(self):
        """ Modification après la première passe"""
        pass
