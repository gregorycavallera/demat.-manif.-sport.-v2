# coding: utf-8

from contacts.models import Contact, Adresse
from legacy.importers.base import BaseImporter
from legacy.util.generic import GenericSelector


class ContactImporter(BaseImporter):
    """ Importer les contacts et adresses """

    # Attributs
    name = "Contacts"
    description = "Importation des contacts et adresses"
    models = ['contacts.contact', 'contacts.adresse']

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # Contacts
        for item in Contact.objects.using(self.source).all():
            target = GenericSelector.get_local_copy(item.content_object)
            item.content_object = None
            item.pk = None
            item.save(using=self.destination)
            item.content_object = target
            item.save(using=self.destination)

        # Adresses
        for item in Adresse.objects.using(self.source).all():
            target = GenericSelector.get_local_copy(item.content_object)
            item.content_object = None
            item.pk = None
            item.save(using=self.destination)
            item.content_object = target
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
