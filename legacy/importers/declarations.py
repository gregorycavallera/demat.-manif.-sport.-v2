# coding: utf-8

from declarations.models import ManifestationDeclaration
from legacy.importers.base import BaseImporter
from legacy.importers.manifestation import ManifestationImporter


class DeclarationImporter(BaseImporter):
    """ Importer les déclarations """

    # Attributs
    name = "Déclarations de Manifestations"
    description = "Importation des déclarations"
    models = ['declarations.manifestationdeclaration']

    # Overrides
    def process(self, processor):
        """ Importer les manifestations """

        # Déclarations
        for item in ManifestationDeclaration.objects.using(self.source).all():
            item.manifestation_id = ManifestationImporter.get_manifestation(item.manifestation.name).pk
            if item.receipt.name:
                item.receipt.name = "{number}/{base}".format(number=self.number, base=item.receipt.name)
            item.pk = None
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
