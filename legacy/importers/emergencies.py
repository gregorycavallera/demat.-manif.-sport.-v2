# coding: utf-8

from emergencies.models import Association1ersSecours, SecoursPublics
from legacy.importers.base import BaseImporter


class EmergencyImporter(BaseImporter):
    """ Importer les informations des personnels d'urgences médicales """

    # Attributs
    name = "Urgences"
    description = "Importation des informations de personnels attitrés aux urgences médicales"
    models = ['emergencies.association1erssecours', 'emergencies.secourspublics']

    # Getter
    @staticmethod
    def get_association1erssecours(name):
        """ Renvoyer un utilisateur selon son username """
        return Association1ersSecours.objects.using(EmergencyImporter.destination).get(name=name)

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # Premiers secours
        for item in Association1ersSecours.objects.using(self.source).all():
            item.pk = None
            item.save(using=self.destination)

        # Secours publics
        for item in SecoursPublics.objects.using(self.source).all():
            item.pk = None
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
