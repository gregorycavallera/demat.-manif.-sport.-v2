# coding: utf-8

from evaluations.models.n2k import N2KEvaluation
from evaluations.models.rnr import RNREvaluation
from legacy.importers.base import BaseImporter
from legacy.importers.manifestation import ManifestationImporter
from legacy.importers.protectedareas import ProtectedAreaImporter
from legacy.util.generic import GenericSelector


class EvaluationImporter(BaseImporter):
    """ Importer les évals """

    # Attributs
    name = "Évaluations"
    description = "Importation des évaluations"
    models = ['evaluations.n2kevaluation', 'evaluations.rnrevaluation']

    # Overrides
    def process(self, processor):
        """ Importer les évals """

        # Natura 2000
        for item in N2KEvaluation.objects.using(self.source).all():
            sites = list(item.sites.all().values_list('name', flat=True))
            target = GenericSelector.get_local_copy(item.content_object)
            item.manifestation_id = ManifestationImporter.get_manifestation(item.manifestation.name).pk
            item.content_object = None
            item.pk = None
            item.save(using=self.destination)
            item.content_object = target
            item.save(using=self.destination)
            item.sites.clear()
            for site in sites:
                item.sites.add(ProtectedAreaImporter.get_site_n2k(site))

        # RNR
        for item in RNREvaluation.objects.using(self.source).all():
            sites = list(item.sites.all().values_list('name', flat=True))
            target = GenericSelector.get_local_copy(item.content_object)
            item.manifestation_id = ManifestationImporter.get_manifestation(item.manifestation.name).pk
            item.content_object = None
            item.pk = None
            item.save(using=self.destination)
            item.content_object = target
            item.save(using=self.destination)
            item.sites.clear()
            for site in sites:
                item.sites.add(ProtectedAreaImporter.get_rnr(site))

    def post_process(self):
        """ Modification après la première passe"""
        pass
