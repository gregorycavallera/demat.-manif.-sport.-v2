# coding: utf-8
from functools import lru_cache

from core.models.instance import Instance
from events.models.autorisationnm import AutorisationNM
from events.models.declarationnm import DeclarationNM
from events.models.documentcomplementaire import DocumentComplementaire
from events.models.manifestation import Manifestation
from events.models.motorizedconcentration import MotorizedConcentration
from events.models.motorizedevent import MotorizedEvent
from events.models.motorizedrace import MotorizedRace
from legacy.importers.base import BaseImporter
from legacy.importers.organisateurs import OrganisateurImporter
from legacy.importers.services import ServiceImporter
from legacy.importers.sports import SportImporter


class ManifestationImporter(BaseImporter):
    """ Importer les manifestations """

    # Attributs
    name = "Manifestations"
    description = "Importation des manifestations (base de 7 autres applications)"
    models = ['events.manifestation']

    # Getter
    @staticmethod
    @lru_cache(maxsize=1024)
    def get_instance(name):
        return Instance.objects.set_for_departement(name)

    @staticmethod
    @lru_cache(maxsize=1024)
    def get_manifestation(name):
        return Manifestation.objects.get(name=name)

    # Overrides
    def process(self, processor):
        """ Importer les manifestations """

        # Avis
        for item in Manifestation.objects.using(self.source).all():
            # Renommer la manifestation lorsqu'une manifestation portant le même nom a déjà été importée
            if Manifestation.objects.using(self.destination).filter(name__iexact=item.name).exists():
                item.name = "{0} ({1})".format(item.name, self.number)
                item.save()

            if item.safety_provisions.name:
                item.safety_provisions.name = "{number}/{base}".format(number=self.number, base=item.safety_provisions.name)
            if item.additional_docs.name:
                item.additional_docs.name = "{number}/{base}".format(number=self.number, base=item.additional_docs.name)
            if item.insurance_certificate.name:
                item.insurance_certificate.name = "{number}/{base}".format(number=self.number, base=item.insurance_certificate.name)
            if item.rounds_safety.name:
                item.rounds_safety.name = "{number}/{base}".format(number=self.number, base=item.rounds_safety.name)
            if item.organisateur_commitment.name:
                item.organisateur_commitment.name = "{number}/{base}".format(number=self.number, base=item.organisateur_commitment.name)
            if item.doctor_attendance.name:
                item.doctor_attendance.name = "{number}/{base}".format(number=self.number, base=item.doctor_attendance.name)
            if item.manifestation_rules.name:
                item.manifestation_rules.name = "{number}/{base}".format(number=self.number, base=item.manifestation_rules.name)
            if item.cartography.name:
                item.cartography.name = "{number}/{base}".format(number=self.number, base=item.cartography.name)
            item.save()

            # Récupérer l'avis précis de service
            # La particularité ici est qu'il est impossible d'identifier un Avis via ses champs (autres que id)
            communes = list(item.crossed_cities.all().values_list('code', 'name'))
            departements = list(item.other_departments_crossed.all().values_list('name', flat=True))
            anm_items = list(AutorisationNM.objects.using(self.source).filter(manifestation_ptr=item))
            dnm_items = list(DeclarationNM.objects.using(self.source).filter(manifestation_ptr=item))
            mc_items = list(MotorizedConcentration.objects.using(self.source).filter(manifestation_ptr=item))
            me_items = list(MotorizedEvent.objects.using(self.source).filter(manifestation_ptr=item))
            mr_items = list(MotorizedRace.objects.using(self.source).filter(manifestation_ptr=item))
            # Sauvegarder l'avis de base
            item.instance_id = ManifestationImporter.get_instance(item.instance.departement.name).pk
            item.structure_id = OrganisateurImporter.get_structure(item.structure.organisateur.user.username).pk
            item.activite_id = SportImporter.get_activite(item.activite.name).pk
            item.departure_city_id = ServiceImporter.get_commune(item.departure_city.code, item.departure_city.name).pk
            item.pk = None
            item.save(using=self.destination)
            item.crossed_cities.clear()
            item.other_departments_crossed.clear()
            for commune in communes:
                item.crossed_cities.add(ServiceImporter.get_commune(commune[0], commune[1]))
            for departement in departements:
                item.other_departments_crossed.add(ServiceImporter.get_departement(departement))

            if anm_items:
                for sub in anm_items:
                    sub.multisport_federation_id = SportImporter.get_multisport_federation(sub.multisport_federation.short_name).pk if \
                        sub.multisport_federation else None
                    sub.id = item.pk
                    sub.instance_id = ManifestationImporter.get_instance(sub.instance.departement.name).pk
                    sub.structure_id = OrganisateurImporter.get_structure(sub.structure.organisateur.user.username).pk
                    sub.activite_id = SportImporter.get_activite(sub.activite.name).pk
                    sub.departure_city_id = ServiceImporter.get_commune(sub.departure_city.code, sub.departure_city.name).pk
                    if sub.signalers_list.name:
                        sub.signalers_list.name = "{number}/{base}".format(number=self.number, base=sub.signalers_list.name)
                    sub.save(using=self.destination)

            if dnm_items:
                for sub in dnm_items:
                    sub.id = item.pk
                    sub.instance_id = ManifestationImporter.get_instance(sub.instance.departement.name).pk
                    sub.structure_id = OrganisateurImporter.get_structure(sub.structure.organisateur.user.username).pk
                    sub.activite_id = SportImporter.get_activite(sub.activite.name).pk
                    sub.departure_city_id = ServiceImporter.get_commune(sub.departure_city.code, sub.departure_city.name).pk
                    sub.save(using=self.destination)

            if mc_items:
                for sub in mc_items:
                    sub.id = item.pk
                    sub.instance_id = ManifestationImporter.get_instance(sub.instance.departement.name).pk
                    sub.structure_id = OrganisateurImporter.get_structure(sub.structure.organisateur.user.username).pk
                    sub.activite_id = SportImporter.get_activite(sub.activite.name).pk
                    sub.departure_city_id = ServiceImporter.get_commune(sub.departure_city.code, sub.departure_city.name).pk
                    sub.save(using=self.destination)

            if mr_items:
                for sub in mr_items:
                    sub.id = item.pk
                    sub.instance_id = ManifestationImporter.get_instance(sub.instance.departement.name).pk
                    sub.structure_id = OrganisateurImporter.get_structure(sub.structure.organisateur.user.username).pk
                    sub.activite_id = SportImporter.get_activite(sub.activite.name).pk
                    sub.departure_city_id = ServiceImporter.get_commune(sub.departure_city.code, sub.departure_city.name).pk
                    if sub.mass_map.name:
                        sub.mass_map.name = "{number}/{base}".format(number=self.number, base=sub.mass_map.name)
                    sub.save(using=self.destination)

            if me_items:
                for sub in me_items:
                    sub.id = item.pk
                    sub.instance_id = ManifestationImporter.get_instance(sub.instance.departement.name).pk
                    sub.structure_id = OrganisateurImporter.get_structure(sub.structure.organisateur.user.username).pk
                    sub.activite_id = SportImporter.get_activite(sub.activite.name).pk
                    sub.departure_city_id = ServiceImporter.get_commune(sub.departure_city.code, sub.departure_city.name).pk
                    if sub.commissioners.name:
                        sub.commissioners.name = "{number}/{base}".format(number=self.number, base=sub.commissioners.name)
                    if sub.hourly_itinerary.name:
                        sub.hourly_itinerary.name = "{number}/{base}".format(number=self.number, base=sub.hourly_itinerary.name)
                    if sub.tech_organisateur_certificate.name:
                        sub.tech_organisateur_certificate.name = "{number}/{base}".format(number=self.number, base=sub.tech_organisateur_certificate.name)
                    if sub.public_zone_map.name:
                        sub.public_zone_map.name = "{number}/{base}".format(number=self.number, base=sub.public_zone_map.name)
                    if sub.delegate_federation_agr.name:
                        sub.delegate_federation_agr.name = "{number}/{base}".format(number=self.number, base=sub.delegate_federation_agr.name)
                    sub.save(using=self.destination)

        # Documents complémentaires
        for item in DocumentComplementaire.objects.using(self.source).all():
            item.manifestation_id = self.get_manifestation(item.manifestation.name).pk
            item.pk = None
            if item.attached_document.name:
                item.attached_document.name = "{number}/{base}".format(number=self.number, base=item.attached_document.name)
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
