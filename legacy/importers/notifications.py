# coding: utf-8

from legacy.importers.base import BaseImporter
from legacy.importers.manifestation import ManifestationImporter
from legacy.importers.users import UserImporter
from legacy.util.generic import GenericSelector
from notifications.models.action import Action
from notifications.models.notification import Notification


class NotificationImporter(BaseImporter):
    """ Importer les enregistrements d'actions et notifications """

    # Attributs
    name = "Notifications"
    description = "Importation des actions et notifications"
    models = ['notifications.action', 'notifications.notification']

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # Actions
        for item in Action.objects.using(self.source).all():
            item.manifestation_id = ManifestationImporter.get_manifestation(item.manifestation.name).pk
            item.user_id = UserImporter.get_user(item.user.username).pk
            item.pk = None
            item.save(using=self.destination)

        # Notifications
        for item in Notification.objects.using(self.source).all():
            target = GenericSelector.get_local_copy(item.content_object, self.number)
            item.content_object = None
            item.manifestation_id = ManifestationImporter.get_manifestation(item.manifestation.name).pk
            item.user_id = UserImporter.get_user(item.user.username).pk
            item.pk = None
            item.save(using=self.destination)
            item.content_object = target
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
