# coding: utf-8
from django.contrib.auth.models import Group

from legacy.importers.base import BaseImporter
from legacy.importers.services import ServiceImporter
from nouveautes.models.nouveaute import Nouveaute


class NouveautesImporter(BaseImporter):
    """ Importer les nouveautés """

    # Attributs
    name = "Nouveautés"
    description = "Importation des nouveautés"  # Facultatif
    models = ['nouveautes.nouveaute']

    # Getter
    @staticmethod
    def get_group(name):
        return Group.objects.get(name=name)

    # Overrides
    def process(self, processor):
        """ Importer les nouveautés """

        # Nouveautés
        for item in Nouveaute.objects.using(self.source).all():
            item.pk = None
            item.save(using=self.destination)
            item.departements.add(ServiceImporter.get_departement(str(self.number)))

    def post_process(self):
        """ Modification après la première passe"""
        pass
