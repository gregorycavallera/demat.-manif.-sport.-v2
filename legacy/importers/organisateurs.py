# coding: utf-8
from functools import lru_cache

from legacy.importers.base import BaseImporter
from legacy.importers.services import ServiceImporter
from legacy.importers.users import UserImporter
from organisateurs.models.organisateur import Organisateur
from organisateurs.models.structure import StructureType, Structure


class OrganisateurImporter(BaseImporter):
    """ Importer les structures et organisateurs """

    # Attributs
    name = "Organisateurs"
    description = "Importation des organisateurs et structures"
    models = ['organisateurs.organisateur', 'organisateurs.structuretype', 'organisateurs.structure']

    # Getter
    @staticmethod
    def get_structuretype(name):
        return StructureType.objects.get(type_of_structure__iexact=name)

    @staticmethod
    @lru_cache(maxsize=512)
    def get_structure(name):
        return Structure.objects.get(organisateur__user__username=name)

    @staticmethod
    @lru_cache(maxsize=512)
    def get_organisateur(name):
        return Organisateur.objects.get(user__username=name)

    # Overrides
    def process(self, processor):
        """ Importer les services """

        # Organisateurs
        for item in Organisateur.objects.using(self.source).all():
            if not Organisateur.objects.filter(user_id=UserImporter.get_user(item.user.username).pk).exists():
                item.pk = None
                item.user_id = UserImporter.get_user(item.user.username).pk
                item.save(using=self.destination)

        # Types de Structures
        for item in StructureType.objects.using(self.source).all():
            if not StructureType.objects.filter(type_of_structure__iexact=item.type_of_structure).exists():
                item.pk = None
                item.save(using=self.destination)

        # Structures
        for item in Structure.objects.using(self.source).all():
            if Structure.objects.filter(name=item.name).exists():
                item.name = "{0} ({1})".format(item.name, self.number)
                item.save()
            item.pk = None
            item.type_of_structure_id = self.get_structuretype(item.type_of_structure.type_of_structure).pk
            item.commune_id = ServiceImporter.get_commune(item.commune.code, item.commune.name).pk
            item.organisateur_id = self.get_organisateur(item.organisateur.user.username).pk
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
