# coding: utf-8
from django.db.utils import IntegrityError

from legacy.importers.base import BaseImporter
from protected_areas.models.n2k import SiteN2K, OperateurSiteN2K
from protected_areas.models.rnr import RNR, AdministrateurRNR


class ProtectedAreaImporter(BaseImporter):
    """ Importer les zones protégées (N2000 et RNR) """

    # Attributs
    name = "Zones protégées"
    description = "Importation des zones protégées"
    models = ['protected_areas.siten2k', 'protected_areas.rnr']

    # Getter
    @staticmethod
    def get_site_n2k(name):
        return SiteN2K.objects.get(name=name)

    @staticmethod
    def get_rnr(name):
        return RNR.objects.get(name=name)

    @staticmethod
    def get_administrateur_rnr(short_name, name, email):
        return AdministrateurRNR.objects.get(short_name=short_name, name=name, email=email)

    @staticmethod
    def get_operateur_n2k(name, email, sitename):
        return OperateurSiteN2K.objects.get(name=name, email=email, site__name=sitename)

    # Overrides
    def process(self, processor):
        """ Importer les comptes """

        # N2K
        for item in SiteN2K.objects.using(self.source).all():
            try:
                item.pk = None
                item.save(using=self.destination)
            except IntegrityError:
                pass

        # RNR
        for item in RNR.objects.using(self.source).all():
            try:
                item.pk = None
                item.save(using=self.destination)
            except IntegrityError:
                pass

        # Opérateur N2K
        for item in OperateurSiteN2K.objects.using(self.source).all():
            if not OperateurSiteN2K.objects.filter(name=item.name, email=item.email, site__name=item.site.name).exists():
                item.pk = None
                item.site_id = self.get_site_n2k(item.site.name).pk
                item.save(using=self.destination)

        # Admin RNR
        for item in AdministrateurRNR.objects.using(self.source).all():
            if not AdministrateurRNR.objects.filter(short_name=item.short_name, name=item.name, email=item.email).exists():
                item.pk = None
                item.rnr_id = self.get_rnr(item.rnr.name).pk
                item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
