# coding: utf-8
from functools import lru_cache

from administration.models.service import CGD, GGD, CG, DDSP, SDIS, CODIS, EDSR, Prefecture, Brigade, Compagnie, CIS, Commissariat, Service, CGService
from administrative_division.models.arrondissement import Arrondissement
from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from legacy.importers.base import BaseImporter


class ServiceImporter(BaseImporter):
    """ Importer les services """

    # Attributs
    name = "Service"
    description = "Importation des services administratifs"
    models = ['administration.ggd', 'administration.cg', 'administration.ddsp', 'administration.sdis', 'administration.codis', 'administration.edsr',
              'administration.prefecture', 'administration.cgd', 'administration.brigade', 'administration.compagnie', 'administration.cis',
              'administration.commissariat', 'administration.service', 'administration.cgservice']

    # Getter
    @staticmethod
    @lru_cache(maxsize=128)
    def get_departement(name):
        return Departement.objects.get(name=name)

    @staticmethod
    @lru_cache(maxsize=512)
    def get_arrondissement(code):
        return Arrondissement.objects.get(code=code)

    @staticmethod
    @lru_cache(maxsize=16384)
    def get_commune(code, name):
        return Commune.objects.get(code=code, name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_cgd(code):
        return CGD.objects.get(arrondissement__code=code)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_brigade(code, ccode, cname, kind):
        return Brigade.objects.get(cgd__arrondissement__code=code, commune__code=ccode, commune__name=cname, kind=kind)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_cis(ciename, cienumber, ccode, cname, name):
        return CIS.objects.get(compagnie__sdis__departement__name=ciename, compagnie__number=cienumber, commune__code=ccode, commune__name=cname, name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_sdis(name):
        return SDIS.objects.get(departement__name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_ddsp(name):
        return DDSP.objects.get(departement__name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_compagnie(name, number):
        return Compagnie.objects.get(sdis__departement__name=name, number=number)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_cg(name):
        return CG.objects.get(departement__name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_cg_service(name, label):
        return CGService.objects.get(cg__departement__name=name, name=label)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_service(name, label):
        return Service.objects.get(departement__name=name, name=label)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_prefecture(code):
        return Prefecture.objects.get(arrondissement__code=code)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_commissariat(code, name):
        return Commissariat.objects.get(commune__code=code, commune__name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_edsr(name):
        return EDSR.objects.get(departement__name=name)

    @staticmethod
    @lru_cache(maxsize=256)
    def get_ggd(name):
        return GGD.objects.get(departement__name=name)

    @staticmethod
    @lru_cache(maxsize=4096)
    def get_by_departement(name, klass):
        return klass.objects.get(departement__name=name)

    @staticmethod
    @lru_cache(maxsize=4096)
    def get_by_arrondissement(code, klass):
        return klass.objects.get(arrondissement__code=code)

    # Overrides
    def process(self, processor):
        """ Importer les services """

        # Traiter les classes de service avec un lien vers un département
        departement_service_classes = [GGD, CG, DDSP, SDIS, CODIS, EDSR, Service]
        for service_class in departement_service_classes:
            for item in service_class.objects.using(self.source).all():
                item.pk = None
                item.departement_id = self.get_departement(item.departement.name).pk
                item.save(using=self.destination)

        # Traiter les classes de service avec un lien vers un arrondissement
        arrondissement_service_classes = [Prefecture, CGD]
        for service_class in arrondissement_service_classes:
            for item in service_class.objects.using(self.source).all():
                item.pk = None
                item.arrondissement_id = self.get_arrondissement(item.arrondissement.code).pk
                item.save(using=self.destination)

        # Traiter les classes de service avec un lien vers une commune
        commune_service_classes = [Commissariat]
        for service_class in commune_service_classes:
            for item in service_class.objects.using(self.source).all():
                item.pk = None
                item.commune_id = self.get_commune(item.commune.code, item.commune.name).pk
                item.save(using=self.destination)

        # Brigade
        for item in Brigade.objects.using(self.source).all():
            item.pk = None
            item.cgd_id = self.get_cgd(item.cgd.arrondissement.code).pk
            item.commune_id = self.get_commune(item.commune.code, item.commune.name).pk
            item.save(using=self.destination)

        # Compagnie
        for item in Compagnie.objects.using(self.source).all():
            item.pk = None
            item.sdis_id = self.get_sdis(item.sdis.departement.name).pk
            item.save(using=self.destination)

        # CIS
        for item in CIS.objects.using(self.source).all():
            item.pk = None
            item.commune_id = self.get_commune(item.commune.code, item.commune.name).pk
            item.compagnie_id = self.get_compagnie(item.compagnie.sdis.departement.name, item.compagnie.number).pk
            item.save(using=self.destination)

        # CGService
        for item in CGService.objects.using(self.source).all():
            item.pk = None
            item.cg_id = self.get_cg(item.cg.departement.name).pk
            item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
