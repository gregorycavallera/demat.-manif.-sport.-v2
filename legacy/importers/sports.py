# coding: utf-8
from django.db import models

from legacy.importers.base import BaseImporter
from legacy.importers.services import ServiceImporter
from sports.models.federation import Federation
from sports.models.sport import Discipline, Activite


class SportImporter(BaseImporter):
    """ Importer les modèles de sports """

    # Attributs
    name = "Sports"
    description = "Importation des fédérations et sports"
    models = ['sports.federation', 'sports.discipline', 'sport.activite']

    # Getter
    @staticmethod
    def get_federation(name):
        return Federation.objects.using(SportImporter.destination).filter(short_name__iexact=name).first()

    @staticmethod
    def get_multisport_federation(name):
        return Federation.objects.using(SportImporter.destination).get(short_name__iexact=name)

    @staticmethod
    def get_discipline(name):
        return Discipline.objects.using(SportImporter.destination).get(name__iexact=name)

    @staticmethod
    def get_activite(name):
        return Activite.objects.using(SportImporter.destination).get(name__iexact=name)

    # Overrides
    def process(self, processor):
        """ Importer les services """

        # Federation
        for item in Federation.objects.using(self.source).all():
            if Federation.objects.filter(short_name__iexact=item.short_name).exists():
                item.short_name += str(self.number)
                item.save()
            item.pk = None
            item.departement_id = ServiceImporter.get_departement(self.number).pk
            item.level = Federation.DEPARTEMENTAL
            item.save(using=self.destination)

        # Discipline
        for item in Discipline.objects.using(self.source).all():
            federation = self.get_federation(item.federation.short_name)
            if not Discipline.objects.filter(name__iexact=item.name).exists():
                item.pk = None
                item.federation_id = None  # vider le champ Fédération
                item.save(using=self.destination)
            try:
                discipline = Discipline.objects.filter(name__iexact=item.name).first()
                federation.disciplines.add(discipline)
            except:
                pass

        # Activité
        for item in Activite.objects.using(self.source).all():
            if not Activite.objects.filter(name__iexact=item.name).exists():
                item.pk = None
                item.discipline_id = self.get_discipline(item.discipline.name).pk
                item.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        for item in Discipline.objects.all():
            # Définir les disciplines nautiques
            if item.name.lower().strip() in ['aviron', 'joutes et barques', 'canoë kayak']:
                item.milieu = Discipline.NAUTIQUE
                item.save()

        # Assigner les fédérations du 42 (celles qui n'ont pas été importées)
        Federation.objects.filter(level=Federation.DEPARTEMENTAL, departement__isnull=True).update(departement=ServiceImporter.get_departement('42'))

        # Supprimer les disciplines qui n'ont pas d'activité
        deleted = Discipline.objects.annotate(act_count=models.Count('activites')).filter(act_count=0).delete()
        print("Attention, {count} disciplines sans activité ont été supprimées.".format(count=deleted[0]))
