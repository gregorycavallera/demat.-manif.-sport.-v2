# coding: utf-8
from functools import lru_cache

from agreements.models.cg import CGAvis
from agreements.models.ddsp import DDSPAvis
from agreements.models.edsr import EDSRAvis
from agreements.models.ggd import GGDAvis
from agreements.models.sdis import SDISAvis
from legacy.importers.base import BaseImporter
from legacy.importers.services import ServiceImporter
from sub_agreements.models.abstract import PreAvis
from sub_agreements.models.cgd import PreAvisCGD
from sub_agreements.models.commissariat import PreAvisCommissariat
from sub_agreements.models.compagnie import PreAvisCompagnie
from sub_agreements.models.edsr import PreAvisEDSR
from sub_agreements.models.servicecg import PreAvisServiceCG


class PreavisImporter(BaseImporter):
    """ Importer les préavis """

    # Attributs
    name = "Préavis"
    description = "Importation des préavis"
    models = ['sub_agreements.preavis']

    # Getter
    @staticmethod
    @lru_cache(maxsize=2048)
    def get_preavis(name):
        return PreAvis.objects.get(avis__authorization__manifestation__name=name)

    # Overrides
    def process(self, processor):
        """ Importer les préavis """

        # Avis
        for item in PreAvis.objects.using(self.source).all():
            # Récupérer le préavis précis de service
            # La particularité ici est qu'il est impossible d'identifier un Avis via ses champs (autres que id)
            cgd_items = list(PreAvisCGD.objects.using(self.source).filter(preavis_ptr=item))
            commissariat_items = list(PreAvisCommissariat.objects.using(self.source).filter(preavis_ptr=item))
            compagnie_items = list(PreAvisCompagnie.objects.using(self.source).filter(preavis_ptr=item))
            edsr_items = list(PreAvisEDSR.objects.using(self.source).filter(preavis_ptr=item))
            servicecg_items = list(PreAvisServiceCG.objects.using(self.source).filter(preavis_ptr=item))
            # Sauvegarder l'avis de base
            item_manifestation_name = item.avis.authorization.manifestation.name
            item.pk = None
            item.save(using=self.destination)
            # CGD
            if cgd_items:
                for sub in cgd_items:
                    services = list(sub.concerned_brigades.all().values_list('cgd__arrondissement__code', 'commune__code', 'commune__name', 'kind'))
                    sub.cgd_id = ServiceImporter.get_cgd(sub.cgd.arrondissement.code).pk
                    target_avis = (
                        GGDAvis.objects.filter(authorization__manifestation__name=item_manifestation_name) or
                        EDSRAvis.objects.filter(authorization__manifestation__name=item_manifestation_name))
                    sub.avis_id = target_avis[0].pk
                    sub.id = item.pk
                    sub.save(using=self.destination)
                    sub.concerned_brigades.clear()
                    for service in services:
                        sub.concerned_brigades.add(ServiceImporter.get_brigade(service[0], service[1], service[2], service[3]))
            # Commissariat
            if commissariat_items:
                for sub in commissariat_items:
                    target_avis = DDSPAvis.objects.filter(authorization__manifestation__name=item.avis.authorization.manifestation.name)
                    sub.avis_id = target_avis.first().pk
                    sub.commissariat_id = ServiceImporter.get_commissariat(sub.commissariat.commune.code, sub.commissariat.commune.name).pk
                    sub.id = item.pk
                    sub.save(using=self.destination)
            # Compagnies
            if compagnie_items:
                for sub in compagnie_items:
                    services = list(sub.concerned_cis.all().values_list('compagnie__sdis__departement__name', 'compagnie__number', 'commune__code',
                                                                        'commune__name', 'name'))
                    sub.compagnie_id = ServiceImporter.get_compagnie(sub.compagnie.sdis.departement.name, sub.compagnie.number).pk
                    target_avis = SDISAvis.objects.filter(authorization__manifestation__name=item.avis.authorization.manifestation.name)
                    sub.avis_id = target_avis.first().pk
                    sub.id = item.pk
                    sub.save(using=self.destination)
                    sub.concerned_cis.clear()
                    for service in services:
                        sub.concerned_cis.add(ServiceImporter.get_cis(service[0], service[1], service[2], service[3], service[4]))
            # EDSR
            if edsr_items:
                for sub in edsr_items:
                    sub.edsr_id = ServiceImporter.get_edsr(sub.edsr.departement.name).pk
                    sub.id = item.pk
                    target_avis = GGDAvis.objects.filter(authorization__manifestation__name=item.avis.authorization.manifestation.name)
                    sub.avis_id = target_avis.first().pk
                    sub.save(using=self.destination)
            # Service CG
            if servicecg_items:
                for sub in servicecg_items:
                    sub.cg_service_id = ServiceImporter.get_cg_service(sub.cg_service.cg.departement.name, sub.cg_service.name).pk
                    sub.id = item.pk
                    target_avis = CGAvis.objects.filter(authorization__manifestation__name=item.avis.authorization.manifestation.name)
                    sub.avis_id = target_avis.first().pk
                    sub.save(using=self.destination)

    def post_process(self):
        """ Modification après la première passe"""
        pass
