# coding: utf-8
from functools import lru_cache

from django.db.utils import IntegrityError

from administration.models.agent import Agent, GGDAgent, EDSRAgent, CODISAgent, SDISAgent, CISAgent, DDSPAgent, FederationAgent, MairieAgent, CGAgent, \
    CGSuperieur, ServiceAgent, BrigadeAgent
from administration.models.agentlocal import AgentLocal, EDSRAgentLocal, CGDAgentLocal, CompagnieAgentLocal, CommissariatAgentLocal, CGServiceAgentLocal
from administration.models.people import Instructeur, Observateur, Secouriste
from administration.models.service import EDSR, CGD, GGD, CODIS, SDIS, DDSP
from administrative_division.models.departement import Departement
from core.models.instance import Instance
from core.models.user import User
from legacy.importers.base import BaseImporter
from legacy.importers.emergencies import EmergencyImporter
from legacy.importers.services import ServiceImporter
from legacy.importers.sports import SportImporter


class UserImporter(BaseImporter):
    """ Importer les utilisateurs """

    # Attributs
    name = "User"
    description = "Importation des utilisateurs et des rôles associés "
    models = ['core.instance', 'core.user', 'organisateurs.organisateur', 'administration.instructeur', 'administration.observateur',
              'administration.secouriste', 'administration.agent', 'administration.ggdagent', 'administration.edsragent', 'administration.brigadeagent',
              'administration.codisagent', 'administration.sdisagent', 'administration.cisagent', 'administration.ddspagent', 'administration.federationagent',
              'administration.mairieagent', 'administration.cgagent', 'administration.cgsuperieur', 'administration.serviceagent']

    # Getter
    @staticmethod
    @lru_cache(maxsize=2048)
    def get_user(username):
        """ Renvoyer un utilisateur selon son username """
        return User.objects.using(UserImporter.destination).get(username=username)

    @staticmethod
    @lru_cache(maxsize=2048)
    def get_observateur(username):
        return Observateur.objects.get(user__username=username)

    @staticmethod
    @lru_cache(maxsize=2048)
    def get_agent(username):
        return Agent.objects.get(user__username=username)

    @staticmethod
    @lru_cache(maxsize=2048)
    def get_agentlocal(username):
        return AgentLocal.objects.get(user__username=username)

    @staticmethod
    @lru_cache(maxsize=2048)
    def get_by_user(username, klass):
        return klass.objects.get(user__username=username)

    # Overrides
    def process(self, processor):
        """ Importer """

        # Importer l'instance de la base
        instance = Instance.objects.configured().using(self.source).first()
        instance.pk = None
        instance.save(using=self.destination)
        instance.departement = Departement.objects.get(name=instance.departement.name)
        instance.save(using=self.destination)

        # Importer les utilisateurs
        for user in User.objects.using(self.source).all():
            # Utilisateur
            if not User.objects.filter(username=user.username).exists():
                user.pk = None
                user.default_instance_id = instance.pk
                user.save(using=self.destination)
            else:
                user.username = "{0}-{1}-{2}".format(user.username, self.number, user.id)
                user.status = User.USERNAME_CHANGED
                user.save()
                user.pk = None
                user.default_instance_id = instance.pk
                user.save(using=self.destination)

        # Traiter les classes de service avec uniquement un lien vers un utilisateur
        user_role_classes = [Observateur]
        for user_role_class in user_role_classes:
            for item in user_role_class.objects.using(self.source).all():
                try:
                    item.pk = None
                    item.user_id = self.get_user(item.user.username).pk
                    item.save(using=self.destination)
                except IntegrityError:
                    pass

        # Instructeurs
        for item in Instructeur.objects.using(self.source).all():
            if not Instructeur.objects.filter(user__username=item.user.username).exists():
                item.pk = None
                item.user_id = self.get_user(item.user.username).pk
                item.prefecture_id = ServiceImporter.get_prefecture(item.prefecture.arrondissement.code).pk
                item.save(using=self.destination)

        # Secouristes
        for item in Secouriste.objects.using(self.source).all():
            item.pk = None
            item.association_id = EmergencyImporter.get_association1erssecours(item.association.name).pk
            item.user_id = self.get_user(item.user.username).pk
            item.save(using=self.destination)

        # AGENTS LOCAUX
        # Local EDSR
        for item in EDSRAgentLocal.objects.using(self.source).all():
            EDSRAgentLocal.objects.create(user=self.get_user(item.user.username), edsr=ServiceImporter.get_by_departement(item.edsr.departement.name, EDSR))

        # Local CGD
        for item in CGDAgentLocal.objects.using(self.source).all():
            CGDAgentLocal.objects.create(user=self.get_user(item.user.username), cgd=ServiceImporter.get_by_arrondissement(item.cgd.arrondissement.code, CGD))

        # Local Compagnie
        for item in CompagnieAgentLocal.objects.using(self.source).all():
            CompagnieAgentLocal.objects.create(user=self.get_user(item.user.username), compagnie=ServiceImporter.get_compagnie(
                item.compagnie.sdis.departement.name, item.compagnie.number))

        # Local Commissariat
        for item in CommissariatAgentLocal.objects.using(self.source).all():
            CommissariatAgentLocal.objects.create(user=self.get_user(item.user.username),
                                                  commissariat=ServiceImporter.get_commissariat(item.commissariat.commune.code, item.commissariat.commune.name))

        # Local Service CG
        for item in CGServiceAgentLocal.objects.using(self.source).all():
            CGServiceAgentLocal.objects.create(user=self.get_user(item.user.username),
                                               cg_service=ServiceImporter.get_cg_service(item.cg_service.cg.departement.name, item.cg_service.name))

        # AGENTS DÉPARTEMENTAUX
        # GGD
        for item in GGDAgent.objects.using(self.source).all():
            GGDAgent.objects.create(user=self.get_user(item.user.username), ggd=ServiceImporter.get_by_departement(item.ggd.departement.name, GGD))

        # EDSR
        for item in EDSRAgent.objects.using(self.source).all():
            EDSRAgent.objects.create(user=self.get_user(item.user.username), edsr=ServiceImporter.get_by_departement(item.edsr.departement.name, EDSR))

        # Brigade
        for item in BrigadeAgent.objects.using(self.source).all():
            BrigadeAgent.objects.create(user=self.get_user(item.user.username),
                                        brigade=ServiceImporter.get_brigade(item.brigade.cgd.arrondissement.code,
                                                                            item.brigade.commune.code,
                                                                            item.brigade.commune.name,
                                                                            item.brigade.kind))

        # CODIS
        for item in CODISAgent.objects.using(self.source).all():
            CODISAgent.objects.create(user=self.get_user(item.user.username), codis=ServiceImporter.get_by_departement(item.codis.departement.name, CODIS))

        # SDIS
        for item in SDISAgent.objects.using(self.source).all():
            SDISAgent.objects.create(user=self.get_user(item.user.username), sdis=ServiceImporter.get_by_departement(item.sdis.departement.name, SDIS))

        # CIS
        for item in CISAgent.objects.using(self.source).all():
            CISAgent.objects.create(user=self.get_user(item.user.username), cis=ServiceImporter.get_cis(item.cis.compagnie.sdis.departement.name,
                                                                                                        item.cis.compagnie.number,
                                                                                                        item.cis.commune.code,
                                                                                                        item.cis.commune.name,
                                                                                                        item.cis.name))

        # DDSP
        for item in DDSPAgent.objects.using(self.source).all():
            DDSPAgent.objects.create(user=self.get_user(item.user.username), ddsp=ServiceImporter.get_by_departement(item.ddsp.departement.name, DDSP))

        # Fédération
        for item in FederationAgent.objects.using(self.source).all():
            FederationAgent.objects.create(user=self.get_user(item.user.username), federation=SportImporter.get_federation(item.federation.short_name))

        # Mairie
        for item in MairieAgent.objects.using(self.source).all():
            MairieAgent.objects.create(user=self.get_user(item.user.username), commune=ServiceImporter.get_commune(item.commune.code, item.commune.name))

        # CG
        for item in CGAgent.objects.using(self.source).all():
            CGAgent.objects.create(user=self.get_user(item.user.username), cg=ServiceImporter.get_cg(item.cg.departement.name))

        # CG Supérieur
        for item in CGSuperieur.objects.using(self.source).all():
            CGSuperieur.objects.create(user=self.get_user(item.user.username), cg=ServiceImporter.get_cg(item.cg.departement.name))

        # Service
        for item in ServiceAgent.objects.using(self.source).all():
            ServiceAgent.objects.create(user=self.get_user(item.user.username), service=ServiceImporter.get_service(item.service.departement.name,
                                                                                                                    item.service.name))

    def post_process(self):
        """ Modification après la première passe"""
        pass
