# coding: utf-8
from django.core.management.base import BaseCommand

from core.util.security import protect_code
from legacy.importers.notifications import NotificationImporter
from legacy.processor.processor import ManifsportProcessor


class Command(BaseCommand):
    """
    Mettre à jour le champ d'instance des manifestations et des utilisateurs d'un serveur

    L'étape est nécessaire pour la fusion des données des différents serveurs déployés
    """
    args = ''
    help = "Ajouter les données des bases de données configurées vers 'default'"

    def add_arguments(self, parser):
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Demander une validation de l'utilisateur")
        parser.add_argument('--no-notifications', action='store_false', dest='notifications', default=True,
                            help="Désactiver l'import des Notifications (notifications.Notification)")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        interactive = options.get('interactive')
        notifications = options.get('notifications')

        reply = ''

        if interactive:
            reply = input("Tenter la fusion dans 'default' des bases configurées ? [oui/*] : ")

        if not interactive or reply.lower() in ("oui", "y", "yes", "o"):
            # Mettre à jour tous les utilisateurs
            processor = ManifsportProcessor()
            # Désactiver l'import des notifications si demandé
            if notifications is False:
                del processor.ordering[processor.ordering.index(NotificationImporter)]
            processor.process()
            print("Terminé\n")
            return

        print("Aucun élément de la base n'a été modifié. Terminé.")
        return
