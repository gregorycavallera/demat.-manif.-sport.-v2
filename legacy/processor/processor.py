# coding: utf-8
from django.conf import settings

from legacy.importers.agreements import AvisImporter
from legacy.importers.aide import AideImporter
from legacy.importers.allauth import AllauthImporter
from legacy.importers.autorisations import AutorisationImporter
from legacy.importers.base import BaseProcessor
from legacy.importers.contacts import ContactImporter
from legacy.importers.declarations import DeclarationImporter
from legacy.importers.emergencies import EmergencyImporter
from legacy.importers.evaluations import EvaluationImporter
from legacy.importers.manifestation import ManifestationImporter
from legacy.importers.notifications import NotificationImporter
from legacy.importers.nouveautes import NouveautesImporter
from legacy.importers.organisateurs import OrganisateurImporter
from legacy.importers.protectedareas import ProtectedAreaImporter
from legacy.importers.services import ServiceImporter
from legacy.importers.sports import SportImporter
from legacy.importers.subagreements import PreavisImporter
from legacy.importers.users import UserImporter


class ManifsportProcessor(BaseProcessor):
    """ Scénario d'import manifsport """

    # Attributs
    ordering = [
        AideImporter,
        SportImporter,
        ServiceImporter,
        EmergencyImporter,
        UserImporter,
        AllauthImporter,
        OrganisateurImporter,
        ProtectedAreaImporter,
        NouveautesImporter,
        ManifestationImporter,
        DeclarationImporter,
        NotificationImporter,
        AutorisationImporter,
        ContactImporter,
        EvaluationImporter,
        AvisImporter,
        PreavisImporter
    ]
    sources = list(settings.DATABASES.keys())[1:]
    destination = 'default'
