# coding: utf-8
from administration.models.service import Service, GGD, CGD, EDSR, Prefecture, SDIS, Compagnie, DDSP, Commissariat, CG, CGService
from administrative_division.models.commune import Commune
from emergencies.models.premierssecours import Association1ersSecours
from events.models.autorisationnm import AutorisationNM
from events.models.declarationnm import DeclarationNM
from events.models.motorizedconcentration import MotorizedConcentration
from events.models.motorizedevent import MotorizedEvent
from events.models.motorizedrace import MotorizedRace
from legacy.importers.emergencies import EmergencyImporter
from legacy.importers.organisateurs import OrganisateurImporter
from legacy.importers.protectedareas import ProtectedAreaImporter
from legacy.importers.services import ServiceImporter
from legacy.importers.sports import SportImporter
from organisateurs.models.structure import Structure
from protected_areas.models.n2k import OperateurSiteN2K, SiteN2K
from protected_areas.models.rnr import AdministrateurRNR, RNR
from sports.models.federation import Federation


class GenericSelector(object):
    """ Récupérer la copie locale de la cible d'une clé générique dans une base distante """

    @staticmethod
    def get_local_copy(instance, number=None):
        """ Trouver l'instance correspondante dans la base de données par défaut """
        if isinstance(instance, AutorisationNM):
            return AutorisationNM.objects.get(manifestation_ptr__name=instance.manifestation_ptr.name)
        elif isinstance(instance, DeclarationNM):
            return DeclarationNM.objects.get(manifestation_ptr__name=instance.manifestation_ptr.name)
        elif isinstance(instance, MotorizedEvent):
            return MotorizedEvent.objects.get(manifestation_ptr__name=instance.manifestation_ptr.name)
        elif isinstance(instance, MotorizedRace):
            return MotorizedRace.objects.get(manifestation_ptr__name=instance.manifestation_ptr.name)
        elif isinstance(instance, MotorizedConcentration):
            return MotorizedConcentration.objects.get(manifestation_ptr__name=instance.manifestation_ptr.name)
        elif isinstance(instance, Service):
            return ServiceImporter.get_service(instance.departement.name, instance.name)
        elif isinstance(instance, GGD):
            return ServiceImporter.get_ggd(instance.departement.name)
        elif isinstance(instance, CGD):
            return ServiceImporter.get_cgd(instance.arrondissement.code)
        elif isinstance(instance, EDSR):
            return ServiceImporter.get_edsr(instance.departement.name)
        elif isinstance(instance, Prefecture):
            return ServiceImporter.get_prefecture(instance.arrondissement.code)
        elif isinstance(instance, Commissariat):
            return ServiceImporter.get_commissariat(instance.commune.code, instance.commune.name)
        elif isinstance(instance, SDIS):
            return ServiceImporter.get_sdis(instance.departement.name)
        elif isinstance(instance, Compagnie):
            return ServiceImporter.get_compagnie(instance.sdis.departement.name, instance.number)
        elif isinstance(instance, DDSP):
            return ServiceImporter.get_ddsp(instance.departement.name)
        elif isinstance(instance, CG):
            return ServiceImporter.get_cg(instance.departement.name)
        elif isinstance(instance, CGService):
            return ServiceImporter.get_cg_service(instance.cg.departement.name, instance.name)
        elif isinstance(instance, Federation):
            return SportImporter.get_federation(instance.short_name)
        elif isinstance(instance, Commune):
            return ServiceImporter.get_commune(instance.code, instance.name)
        elif isinstance(instance, Structure):
            return OrganisateurImporter.get_structure(instance.organisateur.user.username)
        elif isinstance(instance, AdministrateurRNR):
            return ProtectedAreaImporter.get_administrateur_rnr(instance.short_name, instance.name, instance.email)
        elif isinstance(instance, OperateurSiteN2K):
            return ProtectedAreaImporter.get_operateur_n2k(instance.name, instance.email, instance.site.name)
        elif isinstance(instance, SiteN2K):
            return ProtectedAreaImporter.get_site_n2k(instance.name)
        elif isinstance(instance, RNR):
            return ProtectedAreaImporter.get_rnr(instance.name)
        elif isinstance(instance, Association1ersSecours):
            return EmergencyImporter.get_association1erssecours(instance.name)
        elif instance is None:
            return None
        else:
            raise NotImplementedError("{model} n'est pas pris en charge par GenericSelector.".format(model=instance._meta.label))
