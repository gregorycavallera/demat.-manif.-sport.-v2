# coding: utf-8
import html
from smtplib import SMTPRecipientsRefused, SMTPException

import logging
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.core.mail import send_mail, get_connection
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone

from core.util.types import one_line, make_iterable
from notifications.models.base import LogQuerySet


mail_logger = logging.getLogger('smtp')


class NotificationQuerySet(LogQuerySet):
    """ Queryset des notifications """

    # Setter
    def notify_and_mail(self, recipients, subject, target, manifestation, template=None):
        """
        Ajouter une notification pour les destinataires et envoyer un mail

        Les destinataires sont des instances d'objet avec un champ email.
        Cela inclut les utilisateurs, mais aussi les CG, DDSP, Préfectures etc.
        Tous les destinataires peuvent recevoir un email, mais seuls les destinataires
        étant des Utilisateurs peuvent recevoir une notification via le site.

        :param recipients: destinataires
        :type recipients: [User] ou [tout type avec un attribut user]
        :param subject: Titre à intégrer dans le sujet du mail
        :param target: objet cible du mail, ou objet important (au choix)
        :param manifestation: manifestation ciblée par la notification
        :param template: chemin du template utilisé pour rendre le contenu du mail. à utiliser surtout pour les notifications spéciales
        """
        data = {'subject': subject, 'manifestation': manifestation, 'url': settings.MAIN_URL}
        subject = one_line(render_to_string('notifications/mail/subject.txt', data))
        message = render_to_string(template or 'notifications/mail/message.txt', data)
        recipients = make_iterable(recipients)

        for recipient in recipients:
            recipient = getattr(recipient, 'user', recipient)
            # Envoyer un email si le champ email est disponible sur le destinataire
            if hasattr(recipient, 'email'):
                try:
                    sender_email = manifestation.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    email_config = manifestation.get_instance().get_email_settings()
                    connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
                    send_mail(subject=html.unescape(subject), message=html.unescape(message), from_email=sender_email, recipient_list=[recipient.email],
                              connection=connection)

                    # Créer la notification si le destinataire est un utilisateur
                    if isinstance(recipient, get_user_model()):
                        self.create(user=recipient, manifestation=manifestation, subject=subject, content_object=target)

                except SMTPRecipientsRefused:
                    # En cas d'échec où l'adresse n'existe pas (ou similaire), notifier les instructeurs du dossier
                    # À noter que l'échec SMTP ne peut se produire qu'en situation de production
                    # donc à surveiller.
                    mail_logger.exception("notifications.notification.notify_and_mail: SMTP Recipients refused, trying to send to instructors")
                    if not recipient.has_role('instructeur'):  # évite d'éventuels problèmes de récursivité infinie
                        self.notify_and_mail(manifestation.get_autorisation().get_instructeurs(), "problème d'email",
                                             recipient, manifestation, template='notifications/mail/message-mailerror.txt')
                except SMTPException:
                    # Si le serveur SMTP pose un autre type de problème, logger l'erreur
                    exception_text = """
                    notifications.notification.notify_and_mail: Une exception SMTP non gérée vient de se produire
                    instance: {instance}
                    destinataires: {recipients}
                    manifestation: {manifestation}
                    cible: {target}
                    """
                    mail_logger.exception(exception_text.format(instance=manifestation.get_instance(), recipients=recipients,
                                                                manifestation=manifestation, target=target))


class Notification(models.Model):
    """ Notification à un agent concernant une modification sur une manifestation """

    # Champs
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='notifications', verbose_name="utilisateur", on_delete=models.CASCADE)
    creation_date = models.DateTimeField("date", default=timezone.now)
    subject = models.CharField("sujet", max_length=255)
    manifestation = models.ForeignKey("events.manifestation", verbose_name="manifestation", on_delete=models.CASCADE)
    content_type = models.ForeignKey('contenttypes.contenttype', null=True, on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    read = models.BooleanField("lu", default=False)
    objects = NotificationQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return ' - '.join([self.user.username, self.manifestation.name, self.subject])

    # Meta
    class Meta:
        verbose_name = "notification"
        verbose_name_plural = "notifications"
        default_related_name = "notifications"
        app_label = "notifications"
