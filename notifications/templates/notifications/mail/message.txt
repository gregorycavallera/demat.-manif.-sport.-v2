{{subject}} pour la manifestation "{{manifestation}}"

=== VEUILLEZ NE PAS RÉPONDRE À CET E-MAIL ===
Pour effectuer la suite de votre démarche :
- Connectez vous sur la plateforme Manifestation Sportive ;
- rendez-vous sur votre tableau de bord.
Rendez-vous à l'adresse {{url}}
