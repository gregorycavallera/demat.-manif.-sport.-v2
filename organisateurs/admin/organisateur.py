# coding: utf-8
from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import set_admin_info, RelationOnlyFieldListFilter
from organisateurs.models import Organisateur


@admin.register(Organisateur)
class OrganisateurAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Admin des organisateurs """

    # Configuration
    list_display = ['pk', 'user', 'structure__name', 'structure__commune__arrondissement__departement']
    list_filter = ['cgu', ('structure__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username', 'user__first_name', 'user__last_name', 'user__email']
    inlines = []
    actions = ['create_compte_openrunner']

    # Actions
    @set_admin_info(short_description="Créer un compte openrunner pour chaque organisateur sélectionné")
    def create_compte_openrunner(self, request, queryset):
        """ Créer un compte OpenRunner pour l'organistaeur """
        created = 0
        for instance in queryset:
            try:
                if instance.create_openrunner_user() is True:
                    created += 1
            except (AttributeError, ObjectDoesNotExist):
                pass
        self.message_user(request, "{count} organisateurs ont désormais un compte OpenRunner.".format(count=created))

    # Overrides
    def lookup_allowed(self, lookup, value):
        if lookup.startswith('structure__commune__arrondissement__departement'):
            return True
        return super().lookup_allowed(lookup, value)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(structure__commune__arrondissement__departement=request.user.get_departement())
        return queryset
