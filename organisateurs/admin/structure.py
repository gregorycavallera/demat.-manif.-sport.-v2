# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import RelationOnlyFieldListFilter
from organisateurs.forms.structure import StructureForm
from organisateurs.models import Structure, StructureType


@admin.register(StructureType)
class StructureTypeAdmin(ImportExportActionModelAdmin):
    pass


@admin.register(Structure)
class StructureAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des structures organisatrices """

    list_display = ['pk', 'name', 'type_of_structure', 'organisateur', 'commune__arrondissement__departement']
    list_filter = ['type_of_structure__type_of_structure', ('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    form = StructureForm

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset
