# coding: utf-8
from django.shortcuts import render

from core.util.permissions import require_role
from events.models.manifestation import Manifestation


def require_owner(function=None):
    """ Décorateur : limite l'accès aux organisateurs d'un événement """

    def decorator(view_func):
        @require_role('organisateur')
        def _wrapped_view(request, *args, **kwargs):
            if request.user == Manifestation.objects.get(pk=kwargs['pk']).structure.organisateur.user:  # noqa
                return view_func(request, *args, **kwargs)
            else:
                return render(request, "core/access_restricted.html", status=403)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
