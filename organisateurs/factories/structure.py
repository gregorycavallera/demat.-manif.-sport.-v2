# coding: utf-8
import factory
from django.db.models.signals import post_save

from administrative_division.factories import CommuneFactory
from carto.listeners import create_openrunner_user
from organisateurs.models import Structure, StructureType


class StructureTypeFactory(factory.django.DjangoModelFactory):
    """ Factory type de structure """

    # Champs
    type_of_structure = factory.Sequence(lambda n: 'Association {0}'.format(n))

    # Meta
    class Meta:
        model = StructureType


class StructureFactory(factory.django.DjangoModelFactory):
    """ Factory structure """

    # Champs
    name = factory.Sequence(lambda n: 'GRS{0}'.format(n))
    type_of_structure = factory.SubFactory(StructureTypeFactory)
    organisateur = factory.SubFactory('organisateurs.factories.OrganisateurFactory')
    commune = factory.SubFactory(CommuneFactory)
    address = factory.Sequence(lambda n: 'address{0}'.format(n))
    phone = '06 12 23 45 56'
    website = 'http://www.grs.fr'

    # Overrides
    @classmethod
    def _generate(cls, create, attrs):
        """ Override the default _generate() to disable the post-save signal. """
        post_save.disconnect(create_openrunner_user, Structure)
        structure = super(StructureFactory, cls)._generate(create, attrs)
        post_save.connect(create_openrunner_user, Structure)
        return structure

    # Meta
    class Meta:
        model = Structure
