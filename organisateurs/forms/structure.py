# coding: utf-8
from clever_selects.form_fields import ChainedModelChoiceField
from crispy_forms.layout import Submit
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from localflavor.fr.forms import FRPhoneNumberField

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from core.forms.base import GenericForm
from organisateurs.models import Structure


class StructureForm(GenericForm):
    """ Formulaire des structures """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'),
                                      Commune, label="Commune")
    phone = FRPhoneNumberField()

    # Overrides
    def __init__(self, *args, **kwargs):
        super(StructureForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['commune'].empty_label = Commune.objects.get(id=self.initial['commune'])

        self.helper.add_input(Submit('submit', "Enregistrer"))

    def is_valid(self):
        if self.instance.pk:
            if self.data['commune'] == '':
                self.data['commune'] = Commune.objects.get(id=self.initial['commune']).pk
        status = super(StructureForm, self).is_valid()
        return status

    # Meta
    class Meta:
        model = Structure
        fields = ['name', 'organisateur', 'type_of_structure', 'address', 'departement', 'commune', 'phone', 'website']
