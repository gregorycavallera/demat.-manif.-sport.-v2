/*
 * jQuery Live validation
 * Makes a form auto-validated : every form field shows an error if left in a wrong state.
 * The state is fetched from an URL which returns a JSON dictionary (dict)
 * The 'valid' key is a boolean which should tell if the whole form is valid.
 * If not, the dict should contain other keys, amongst which '_all_' represents the list of global form errors
 * the remaining keys are the field ids, and the associated errors.
 * The plugin populates tags with the [field_id]_errors id and [errors_id] class.
 * The refresh occurs when an input field loses the focus, and only updates the tag for the blurred input.
 *
 */
(function ($) {
    $.fn.liveValidate = function (url, settings) {
        settings = $.extend({fields: false, event: 'blur keyup', errors_id: 'errors', errors_class: 'error', submitHandler: null, delay: 250}, settings);
        var element_count, errors_container;
        // Lors de la perte de focus des enfants input de l'élément
        this.find('input, textarea, select').on(settings.event, function (event) {
            // Envoyer une requête AJAX-POST vers l'URL contenant les données du formulaire
            var data = $(this).parents('form').serialize();
            if (settings.fields) {
                data += '&' + $.param(settings.fields);
            }
            setTimeout(function () { // Retarder la vérification (en cas de submit ne pas vérifier)
                $.ajax({
                           url: url, type: 'POST', data: data,
                           async: false, datatype: 'json',
                           error: function (XHR, status, err) {
                           },
                           success: function (data, status) {
                               // Ici, on lit le contenu JSON dans une variable
                               var info = $.parseJSON(data);
                               var valid = info.valid;
                               var input_id = $(event.target).attr('id');
                               if (valid === false) {
                                   // Parcourir les clés du dictionnaire JSON à la recherche de l'ID du contrôle à l'origine de l'événement
                                   for (var key in info) {
                                       errors_container = $("#" + key + "_" + settings.errors_id);
                                       if (key == input_id || (input_id && input_id.indexOf(key + "_") != -1)) {
                                           console.log(errors_container);
                                           if (info[key].length > 0) {
                                               // Créer une liste des erreurs
                                               errors_container.html('<ul class="' + settings.errors_class + '"></ul>');
                                               var error_list = $("#" + key + "_" + settings.errors_id + " ul");
                                               $.each(info[key], function (err, text) {
                                                   $("<li/>").text(text).appendTo(error_list);
                                               });
                                               $(event.target).addClass(settings.errors_class);
                                           } else {
                                               errors_container.html('');
                                               $(event.target).removeClass(settings.errors_class);
                                           }
                                       }
                                   }
                               } else {
                                   // Nettoyer l'éventuelle liste d'erreurs
                                   do {
                                       // Les contrôles composés de django sont spéciaux :
                                       // Si une erreur se produit à cause d'un sous-contrôle (ex. id_birth_day)
                                       // L'erreur est propagée au contrôle parent (ex. id_birth)
                                       // Donc si aucun emplacement id_birth_day n'existe, on essaie de remonter les underscores,
                                       // en essayant id_birth et id jusqu'à trouver un élément existant
                                       errors_container = $("#" + input_id + "_" + settings.errors_id);
                                       element_count = errors_container.size();
                                       input_id = input_id.substring(0, input_id.lastIndexOf('_'));
                                   } while (element_count === 0 && input_id.indexOf('_') !== -1);
                                   errors_container.html('');
                                   $(event.target).parent().parent().find('.' + settings.errors_id).html('');
                                   $(event.target).removeClass(settings.errors_class);
                               }
                           }
                       });
            }, settings.delay);
        });
    };
})(jQuery);
