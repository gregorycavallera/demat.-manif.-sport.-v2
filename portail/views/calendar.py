# coding: utf-8
from django.db.models import Count
from django.utils import timezone
from django.views.generic import ListView

from core.models.instance import Instance
from events.forms import *
from events.models import *


class Calendar(ListView):
    """ Calendrier des manifestations """

    # Configuration
    model = Manifestation
    template_name = 'portail/calendar.html'

    # Overrdes
    def get_queryset(self):
        now = timezone.now()
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement()
        to_display = Manifestation.objects.filter(
            ~Q(autorisationnm__manifestationautorisation__id=None) |
            ~Q(declarationnm__manifestationdeclaration__id=None) |
            ~Q(motorizedevent__manifestationautorisation__id=None) |
            ~Q(motorizedrace__manifestationautorisation__id=None) |
            ~Q(motorizedconcentration__manifestationautorisation__id=None) |
            ~Q(motorizedconcentration__manifestationdeclaration__id=None),
            begin_date__gt=now,
        ).order_by('begin_date')
        # Afficher les manifs du département, ou aucune sur le domaine master
        if departement is not None:
            to_display = to_display.filter(departure_city__arrondissement__departement=departement)
        else:
            to_display = to_display.none()
        return to_display

    def get_context_data(self, **kwargs):
        now = timezone.now()
        context = super(Calendar, self).get_context_data(**kwargs)
        # manif = Count('manifestations', filter=Q(manifestations__instance=Instance.objects.get_for_request(self.request)))
        liste1 = Activite.objects.filter(
            Q(manifestations__begin_date__gt=now) &
            Q(manifestations__hidden=False) &
            Q(manifestations__private=False) &
            Q(manifestations__departure_city__arrondissement__departement__name__icontains='42') &
            Q(manifestations__manifestationautorisation__id__isnull=False))
        liste2 = Activite.objects.filter(
            Q(manifestations__begin_date__gt=now) &
            Q(manifestations__hidden=False) &
            Q(manifestations__private=False) &
            Q(manifestations__departure_city__arrondissement__departement__name__icontains='42') &
            Q(manifestations__manifestationdeclaration__id__isnull=False))
        liste = liste1.union(liste2)
        context['activities'] = liste.order_by('name')
        return context


class FilteredCalendar(Calendar):
    """ Calendrier des manifestations par activité """

    # Overrides
    def get_queryset(self):
        return super(FilteredCalendar, self).get_queryset().filter(activite=self.kwargs['activite'])

    def get_context_data(self, **kwargs):
        context = super(FilteredCalendar, self).get_context_data(**kwargs)
        context['current_activite'] = int(self.kwargs['activite'])
        return context
