# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from contacts.admin import ContactInline, AddressInline
from core.util.admin import RelationOnlyFieldListFilter
from ..models import SiteN2K, OperateurSiteN2K


@admin.register(OperateurSiteN2K)
class OperateurSiteN2KAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'email', 'site', 'get_departement']
    list_filter = ['site', ('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name', 'site__name']
    inlines = [AddressInline, ContactInline]
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset


@admin.register(SiteN2K)
class N2KAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration Natura 2000 """

    list_display = ['pk', 'name', 'site_type', 'index', 'get_departement']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name', 'index']
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset
