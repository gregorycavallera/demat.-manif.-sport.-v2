# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from contacts.admin import ContactInline, AddressInline
from core.util.admin import RelationOnlyFieldListFilter
from ..models import RNR, AdministrateurRNR


@admin.register(AdministrateurRNR)
class AdministrateurRNRAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'rnr']
    list_filter = ['rnr', ('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name', 'rnr__name']
    inlines = [AddressInline, ContactInline]
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset


@admin.register(RNR)
class RNRAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration RNR """

    list_display = ['code', 'name']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset
