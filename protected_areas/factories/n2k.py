# coding: utf-8
import factory

from administrative_division.factories import DepartementFactory
from ..models import SiteN2K, OperateurSiteN2K


class OperateurSiteN2KFactory(factory.django.DjangoModelFactory):
    """ Factory opérateur de site Natura 2000 """

    # Champs
    name = "Parc Naturel Regional du Livradois-Forez"
    email = 'john.doe@parc-livradois-forez.org'
    departement = factory.SubFactory(DepartementFactory)

    # Meta
    class Meta:
        model = OperateurSiteN2K


class SiteN2KFactory(factory.django.DjangoModelFactory):
    """ Factory site Natura 2000 """

    # Champs
    name = "Bois-Noirs"
    site_type = 's'
    index = 'FR8301045'
    departement = factory.SubFactory(DepartementFactory)

    # Méta
    class Meta:
        model = SiteN2K
