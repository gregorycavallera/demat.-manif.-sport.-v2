# coding: utf-8
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from administrative_division.mixins.divisions import DepartementableMixin, ManyPerDepartementQuerySetMixin


class SiteN2KQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(name=name)


class SiteN2K(DepartementableMixin):
    """ Site Natura 2000 """

    # Choix
    TYPE_CHOICES = (('s', 'SIC'), ('z', 'ZPS'))

    # Champs
    name = models.CharField("nom", max_length=255, unique=True)
    site_type = models.CharField("type", max_length=1, choices=TYPE_CHOICES)
    index = models.CharField("fiche", max_length=9, unique=True)
    objects = SiteN2KQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        return self.name,

    # Méta
    class Meta:
        verbose_name = "site natura 2000"
        verbose_name_plural = "sites natura 2000"
        default_related_name = "sitesn2k"
        app_label = "protected_areas"


class OperateurSiteN2KQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, site, name):
        return self.get(site=site, name=name)


class OperateurSiteN2K(DepartementableMixin):
    """ Opérateur de site Natura 2000 """

    # Champs
    name = models.CharField("nom", max_length=255)
    address = GenericRelation("contacts.adresse", verbose_name="adresse")
    email = models.EmailField('e-mail', max_length=200)
    person_in_charge = GenericRelation("contacts.contact", verbose_name="chargé de mission")
    site = models.OneToOneField("protected_areas.siten2k", related_name="operateursiten2k", verbose_name="site natura 2000", on_delete=models.CASCADE)
    objects = OperateurSiteN2KQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        return self.site, self.name

    # Meta
    class Meta:
        verbose_name = "opérateur de site natura 2000"
        verbose_name_plural = "opérateurs de site natura 2000"
        default_related_name = "operateurssitesn2k"
        app_label = "protected_areas"
