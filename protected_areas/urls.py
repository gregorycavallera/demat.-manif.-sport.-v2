# coding: utf-8
from django.urls import path

from .views import SiteN2KList
from .views import RNRList


app_name = 'protected_areas'
urlpatterns = [

    # Listes des RNR et sites Natura2000
    path('RNR/', RNRList.as_view(), name='rnr_list'),
    path('SiteN2K/', SiteN2KList.as_view(), name='siten2k_list'),
]
