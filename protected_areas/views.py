# coding: utf-8
from django.views.generic import ListView

from protected_areas.models import RNR
from protected_areas.models import SiteN2K


class RNRList(ListView):
    """ Afficher la liste des réserves naturelles régionales """

    # Configuration
    model = RNR


class SiteN2KList(ListView):
    """ Afficher la liste des sites Natura 2000 """

    # Configuration
    model = SiteN2K
