# coding: utf-8
from __future__ import absolute_import

from django import template

from sports.models.sport import Activite


register = template.Library()


@register.filter
def incoming_manifestation_count(value, request=None):
    if isinstance(value, Activite):
        return value.get_incoming_manifestation_count(request)
    return -1
