# coding: utf-8
from django.contrib import admin

from ..models import PreAvisEDSR


class EDSRPreAvisInline(admin.StackedInline):
    """ Inline Préavis """

    # Configuration
    model = PreAvisEDSR
    extra = 0
