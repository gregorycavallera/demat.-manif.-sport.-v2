# coding: utf-8
import factory

from administration.factories import CommissariatFactory
from agreements.factories import DDSPAvisFactory
from ..models import PreAvisCommissariat


class PreAvisCommissariatFactory(factory.django.DjangoModelFactory):
    """ Factory des préavis commissariat """

    avis = factory.SubFactory(DDSPAvisFactory)
    commissariat = factory.SubFactory(CommissariatFactory)

    class Meta:
        model = PreAvisCommissariat
