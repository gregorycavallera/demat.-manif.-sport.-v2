# coding: utf-8
import factory

from administration.factories import CompagnieFactory
from agreements.factories import SDISAvisFactory
from ..models import PreAvisCompagnie


class PreAvisCompagnieFactory(factory.django.DjangoModelFactory):
    """ Factory des préavis de compagnie """

    avis = factory.SubFactory(SDISAvisFactory)
    compagnie = factory.SubFactory(CompagnieFactory)

    class Meta:
        model = PreAvisCompagnie
