# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver

from administration.models import CGD
from agreements.models import EDSRAvis
from agreements.models import GGDAvis
from sub_agreements.models.cgd import PreAvisCGD


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=PreAvisCGD)
    def notify_pre_avis_cgd(created, instance, **kwargs):
        if created:
            instance.notify_creation(agents=instance.get_agents(),
                                     content_object=instance.get_edsr())


    @receiver(m2m_changed, sender=EDSRAvis.concerned_cgd.through)
    def create_pre_avis_cgd_1(sender, instance, action, pk_set, **kwargs):
        if action == 'post_add':
            for pk in pk_set:
                avis, _ = PreAvisCGD.objects.get_or_create(avis=instance, cgd=CGD.objects.get(pk=pk))


    @receiver(m2m_changed, sender=GGDAvis.concerned_cgd.through)
    def create_pre_avis_cgd_2(sender, instance, action, pk_set, **kwargs):
        if action == 'post_add':
            for pk in pk_set:
                avis, _ = PreAvisCGD.objects.get_or_create(avis=instance, cgd=CGD.objects.get(pk=pk))
