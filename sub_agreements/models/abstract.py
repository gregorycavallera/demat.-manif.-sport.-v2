# coding: utf-8
from django.db import models
from django.utils import timezone

from administration.models.agentlocal import AgentLocal
from agreements.models import AvisBase
from notifications.models.action import Action
from notifications.models.notification import Notification


class PreAvisQuerySet(models.QuerySet):
    """ Queryset de base des préavis """

    # Getter
    def to_process(self):
        """ Renvoyer les préavis dont la date de manifestation n'est pas encore passée """
        return self.filter(avis__authorization__manifestation__end_date__gte=timezone.now())

    def finished(self):
        """ Renvoyer les préavis dont la date de manifestation est  passée """
        return self.filter(avis__authorization__manifestation__end_date__lt=timezone.now()).order_by('-avis__authorization__manifestation__begin_date')

    def by_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(avis__authorization__manifestation__instance_id=request.user.get_instance().pk)
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(avis__authorization__manifestation__instance__in=instances)
        else:
            return self


class PreAvis(AvisBase):
    """ Modèle de base des préavis """

    # Champs
    avis = models.ForeignKey("agreements.avis", verbose_name="avis", on_delete=models.CASCADE)
    objects = PreAvisQuerySet.as_manager()

    # Action
    def log_ack(self, agents):
        """ Enregistrer l'action préavis rendu """
        recipients = AgentLocal.users_from_agents(agents)
        Action.objects.log(recipients, "pré-avis rendu", self.avis.get_manifestation())

    def notify_creation(self, agents, content_object):
        """ Notifier de la création du préavis """
        recipients = AgentLocal.users_from_agents(agents)
        Notification.objects.notify_and_mail(recipients, "pré-avis demandés", content_object, self.avis.get_manifestation())

    def notify_ack(self, agents, content_object, non_agent_recipient=None):
        """ Notifier du rendu du préavis """
        recipients = AgentLocal.users_from_agents(agents) + [non_agent_recipient]
        Notification.objects.notify_and_mail(recipients, "pré-avis rendu", content_object, self.avis.get_manifestation())

    def notify_publication(self, agents):
        """ Notifier de la publication de l'arrêté """
        prefecture = self.avis.authorization.get_concerned_prefecture()
        recipients = AgentLocal.users_from_agents(agents) + [prefecture]
        Notification.objects.notify_and_mail(recipients, "arrêté d'autorisation publié", prefecture, self.avis.get_manifestation())

    # Meta
    class Meta:
        default_related_name = "preavis"
        app_label = "sub_agreements"
