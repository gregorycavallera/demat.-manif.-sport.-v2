# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from sub_agreements.models.abstract import PreAvis, PreAvisQuerySet


class PreAvisCommissariat(PreAvis):
    """ Préavis commissariat """

    # Champs
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True, related_name='preaviscommissariat', on_delete=models.CASCADE)
    commissariat = models.ForeignKey("administration.commissariat", verbose_name="commissariat", on_delete=models.CASCADE)
    objects = PreAvisQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer le représentation de l'objet """
        manifestation = self.avis.get_manifestation()
        commissariat = self.commissariat
        return ' - '.join([str(manifestation), str(commissariat)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'objet """
        url = 'sub_agreements:commissariat_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_ddsp(self):
        """ Renvoyer le DDSP du préavis """
        return self.commissariat.commune.arrondissement.departement.ddsp

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        return self.commissariat.commissariatagentslocaux.all()

    # Action
    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre le préavis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(agents=self.get_ddsp().ddspagents.all(), content_object=self.commissariat, non_agent_recipient=self.get_ddsp())
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = "pré-avis commissariat"
        verbose_name_plural = "pré-avis commissariats"
        default_related_name = "preaviscommissariat_set"
        app_label = "sub_agreements"
