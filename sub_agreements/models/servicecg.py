# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from sub_agreements.models.abstract import PreAvis, PreAvisQuerySet


class PreAvisServiceCG(PreAvis):
    """ Préavis service Conseil départemental """

    # Champs
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True, related_name='preavisservicecg', on_delete=models.CASCADE)
    cg_service = models.ForeignKey("administration.cgservice", verbose_name="service CD", on_delete=models.CASCADE)
    objects = PreAvisQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        manifestation = self.avis.get_manifestation()
        cg_service = self.cg_service
        return ' - '.join([str(manifestation), str(cg_service)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'objet """
        url = 'sub_agreements:cgservice_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        return self.cg_service.cgserviceagentslocaux.all()

    # Action
    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre le préavis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(agents=self.cg_service.cg.cgagents.all(), content_object=self.cg_service, non_agent_recipient=self.cg_service.cg)
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = "pré-avis service CD"
        verbose_name_plural = "pré-avis services CD"
        default_related_name = "preavisservicecg_set"
        app_label = "sub_agreements"
