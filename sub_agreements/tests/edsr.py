# coding: utf-8

from django.urls import reverse

from agreements.factories import GGDAvisFactory
from core.models.instance import Instance
from sub_agreements.factories import PreAvisEDSRFactory
from sub_agreements.tests.base import PreavisTestsBase


class PreAvisEDSRTests(PreavisTestsBase):
    """ Tests pour les préavis EDSR """

    # Configuration
    def setUp(self):
        super().setUp()
        self.avis = GGDAvisFactory.create(authorization=self.authorization, concerned_edsr=self.edsr)

    # Tests
    def test_str(self):
        """ Tester les représentations chaîne """
        self.instance.workflow_ggd = Instance.WF_GGD_SUBEDSR  # Choisir le seul workflow utilisant les préavis EDSR
        self.instance.save()  # Le choix de workflow ne devrait pas être nécessaire ici
        preavis = PreAvisEDSRFactory.create(avis=self.avis, edsr=self.edsr)
        self.assertEqual(str(preavis), ' - '.join([str(self.manifestation), str(preavis.edsr)]))

    def test_access_url(self):
        """ Tester les URLs """
        self.instance.workflow_ggd = Instance.WF_GGD_SUBEDSR  # Choisir le seul workflow utilisant les préavis EDSR
        self.instance.save()  # Le choix de workflow ne devrait pas être nécessaire ici
        preavis = PreAvisEDSRFactory.create(avis=self.avis, edsr=self.edsr)
        self.assertEqual(preavis.get_absolute_url(), reverse('sub_agreements:edsr_subagreement_detail', kwargs={'pk': preavis.pk}))

    def test_workflow_subedsr(self):
        """ Tester la présence de préavis EDSR dans le workflow GGD avec préavis EDSR """
        # Ici, on a un préavis EDSR qui a été créé : par défaut, Instance est en workflow avec préavis EDSR.
        self.instance.workflow_ggd = Instance.WF_GGD_SUBEDSR  # Choisir le seul workflow utilisant les préavis EDSR
        self.instance.save()
        self.avis.save()  # (en temps normal : S'assurer que le préavis EDSR sera créé automatiquement, maintenant que le workflow est choisi)
        self.assertEqual(self.avis.get_preavis_count(), 1)  # Préavis EDSR
        self.avis.concerned_cgd.add(self.cgd)  # Cela doit créer automatiquement un préavis CGD
        self.assertEqual(self.avis.get_preavis_count(), 2)  # Préavis EDSR + Préavis CGD

    def test_workflow_ggd(self):
        """ Tester la présence de préavis EDSR dans le workflow GGD avec préavis EDSR """
        # Pour simplifier le test, on efface juste les préavis EDSR
        self.avis.get_preavis().delete()
        self.instance.workflow_ggd = Instance.WF_GGD_EDSR  # Choisir un workflow sans préavis EDSR
        self.instance.save()
        self.avis.save()  # Aucun préavis EDSR à créer.
        self.assertEqual(self.avis.get_preavis_count(), 0)  # Aucun préavis
        self.avis.concerned_cgd.add(self.cgd)  # Cela doit créer automatiquement un préavis CGD
        self.assertEqual(self.avis.get_preavis_count(), 1)  # Préavis CGD
