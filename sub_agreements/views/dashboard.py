# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from core.util.permissions import require_role
from ..models import *


class Dashboard(ListView):
    """ Dashboard des préavis """

    # Configuration
    model = PreAvis

    # Overrides
    @method_decorator(require_role('agentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        agentlocal = self.request.user.agentlocal
        user = self.request.user
        request = self.request

        if user.has_role('compagnieagentlocal'):
            return PreAvisCompagnie.objects.by_instance(request=request).to_process().filter(compagnie_id=agentlocal.compagnieagentlocal.compagnie.pk)
        elif user.has_role('cgdagentlocal'):
            return PreAvisCGD.objects.by_instance(request=request).to_process().filter(cgd=agentlocal.cgdagentlocal.cgd)
        elif user.has_role('commissariatagentlocal'):
            return PreAvisCommissariat.objects.by_instance(request=request).to_process().filter(commissariat=agentlocal.commissariatagentlocal.commissariat)
        elif user.has_role('cgserviceagentlocal'):
            return PreAvisServiceCG.objects.by_instance(request=self.request).to_process().filter(cg_service=agentlocal.cgserviceagentlocal.cg_service)
        elif user.has_role('edsragentlocal'):
            return PreAvisEDSR.objects.by_instance(request=self.request).to_process().filter(edsr=agentlocal.edsragentlocal.edsr)
        else:
            return PreAvis.objects.none()

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context
